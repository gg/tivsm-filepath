/* watch.c -- Watch / Rule / Action functions
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
WATCH.C

The definitions of the callers rules are held in a Watch_Item.

watch_add          - Called from jbb.c parse_rule to start a new watch.  Rule setup.
watch_setInfo      - Called from jbb.c parse_action to set watch parameters. Action setup.
watch_getInfo      - Called form jbb.c jbb_doLog, which then writes to the audit buffer.
watch_check        - Called from jbb.c jbb_checkWatchList, which is called from the hook functions.
watch_removeByPool - Called from pf.c if a process has exitined abnormally.

*/

#include <linux/list.h>
#include "jbb.h"

#define POOL_STR_LENGTH 256
const char *WatchType2str[]={"File","Dir"};

typedef struct _Watch_Item {
    struct list_head  list;
    char              watch[WATCH_NAME_LEN];      // Rule name passed in by the rule command
    char              paramList[WATCH_PARAM_LEN]; // Passed in by the action command
    WatchType_e       type;        // Dir or File type
    int               pool;        // associated logging pool for watch
    int               waitIfFull;  // audit log waits if buffer full
    char              name[1];     // variable length array - name being watched
} Watch_Item;

static LIST_HEAD(Watch_Head);
static char *Watch_Mutex = NULL;

// Called from module_init in hook.c
FPRESULT watch_init(void) 
{
    FPRESULT result = 0;

    if (Watch_Mutex) return 0; // Already setup
    Watch_Mutex=MEM_malloc(-8,PF_SEM_SIZE,"wamu");
    if (!Watch_Mutex) return FP_ERR_NOMEM;
    result=PF_MutexInit(PF_SemSystem, Watch_Mutex);
    if (result) {
        LOG_Log(LOG_ERROR,"Watch_Init: failed mutex init:%d\n",result);
        return result;
    }
    return result;
}

void watch_fini(void) 
{
    Watch_Item *watch_item = NULL, *next = NULL;
    if (Watch_Mutex) {
        PF_MutexHold(Watch_Mutex);
        if(!list_empty(&Watch_Head)){
            list_for_each_entry_safe(watch_item, next, &Watch_Head, list) {
                list_del(&watch_item->list);
                MEM_free(watch_item);
            }
        }
        PF_MutexRelease(Watch_Mutex);
        PF_MutexDestroy(Watch_Mutex);
        MEM_free(Watch_Mutex);
        Watch_Mutex = NULL;
    }
}

FPRESULT watch_add(char *watch, char *name, WatchType_e type)
{
    Watch_Item *watch_item = NULL;
    FPRESULT result = 0;
    int namelen = 0, found = _FALSE;

    if (watch == NULL || name == NULL) return FP_ERR_BADPARAM;
    PF_MutexHold(Watch_Mutex);
    if(!list_empty(&Watch_Head)){
        list_for_each_entry(watch_item, &Watch_Head, list) {
            if (watch_item->watch != NULL && watch != NULL) {
                if (strcmp(watch_item->watch, watch)== 0) {
                    found = _TRUE;
                    result = FP_ERR_DUPLICATE_WATCH;
                    break;  // Already exists
                }
            }
        }
    }
    if (found == _FALSE) {
        namelen = strlen(name);
        watch_item = MEM_malloc(-8,sizeof(*watch_item)+namelen,"watc");
        if (!watch_item) result = FP_ERR_NOMEM;
        else {
            g_jbbStats.watchDepth++;
            TRACE_Log(TRACE_WATCH,"watch_add: Adding watch [%s] name [%s] type [%s]\n",watch,name,WatchType2str[type]);
            strncpy(watch_item->watch, watch, WATCH_NAME_LEN);
            strncpy(watch_item->name, name, namelen);
            watch_item->type = type;
            watch_item->pool = WATCH_POOL_UNDEFINED;
            list_add(&watch_item->list, &Watch_Head);
        }
    }

    PF_MutexRelease(Watch_Mutex);
    return result;
}


FPRESULT watch_remove(char *watch, int *pool)
{
    Watch_Item *watch_item = NULL;
    FPRESULT result=0;

    if (watch == NULL || pool == NULL) return FP_ERR_BADPARAM;
    PF_MutexHold(Watch_Mutex);
    if(!list_empty(&Watch_Head)){
        list_for_each_entry(watch_item, &Watch_Head, list) {
            if (watch_item->watch != NULL && watch != NULL) {
                if (strcmp(watch_item->watch, watch) == 0) {
                    g_jbbStats.watchDepth--;
                    TRACE_Log(TRACE_WATCH,"watch_remove: Removing watch [%s]\n",watch);
                    *pool = watch_item->pool;
                    list_del(&watch_item->list);
                    MEM_free(watch_item);
                    break;
                }
            }
        }
    }
    PF_MutexRelease(Watch_Mutex);
    return result;
}


// Not the normal way to purge a watch.  Used by PF_monitorFunc if watcher dies.
FPRESULT watch_removeByPool(int pool)
{
    Watch_Item *watch_item = NULL;
    FPRESULT result=0;

    if ((result = watch_init())!=0) return result; 
    PF_MutexHold(Watch_Mutex);
    if(!list_empty(&Watch_Head)){
        list_for_each_entry(watch_item, &Watch_Head, list) {
            if (watch_item->pool == pool) {
                g_jbbStats.watchDepth--;
                TRACE_Log(TRACE_WATCH,"watch_removeByPool: Removing watch [%s]\n",watch_item->watch);
                list_del(&watch_item->list);
                MEM_free(watch_item);
                break;
            }
        }
    }
    PF_MutexRelease(Watch_Mutex);
    return result;
}


PF_Bool watch_check(char *name,  WatchType_e type)
{
    Watch_Item *watch_item = NULL;
    PF_Bool result = _FALSE;

    if (name == NULL) return result;
    PF_MutexHold(Watch_Mutex);
    if(!list_empty(&Watch_Head)){
        list_for_each_entry(watch_item, &Watch_Head, list) {
            if (watch_item->name != NULL && name != NULL) {
                if ((watch_item->pool != WATCH_POOL_UNDEFINED) &&
                    (watch_item->type == type) &&
                    (strstr(name, watch_item->name) != NULL)) {
                        TRACE_Log(TRACE_WATCH,"watch_check: Check found name [%s] type:[%s] pool:%d\n",name,WatchType2str[type],watch_item->pool);
                        result = _TRUE;
                        break;
                }
            }
        }
    }
    PF_MutexRelease(Watch_Mutex);
    return result;
}


PF_Bool watch_verifyByPool(int pool)
{
    Watch_Item *watch_item = NULL;
    PF_Bool result = _FALSE;

    PF_MutexHold(Watch_Mutex);
    if(!list_empty(&Watch_Head)) {
        list_for_each_entry(watch_item, &Watch_Head, list) {
            if  (watch_item->pool == pool) {
                TRACE_Log(TRACE_WATCH,"watch_verifyByPool: Check found name [%s] type:[%s] pool:%d\n",watch_item->name,WatchType2str[watch_item->type],watch_item->pool);
                result = _TRUE;
                break;
            }
        }
    }
    PF_MutexRelease(Watch_Mutex);
    return result;
}


FPRESULT watch_setInfo(int pool, int waitIfFull, char *paramList)
{
    Watch_Item *watch_item;
    char poolStr[POOL_STR_LENGTH];
    FPRESULT result=FP_ERR_NO_WATCH_MATCH;

    if (pool == WATCH_POOL_UNDEFINED || paramList == NULL) return FP_ERR_BADPARAM;
    TRACE_Log(TRACE_WATCH,"watch_setInfo: pool %d waitIfFull %d paramList [%s]\n",pool,waitIfFull,paramList);
    snprintf(poolStr,POOL_STR_LENGTH,"%d",pool);
    PF_MutexHold(Watch_Mutex);
    if(!list_empty(&Watch_Head)){
        list_for_each_entry(watch_item, &Watch_Head, list) {
            if (watch_item->name != NULL) {
                if ((watch_item->pool == WATCH_POOL_UNDEFINED) &&
                    (strstr(watch_item->watch, poolStr) != NULL)) {
                        TRACE_Log(TRACE_WATCH,"watch_setPool: watch [%s] name [%s] type:[%s] pool:%d wait: %d param:[%s]\n",
                            watch_item->watch, watch_item->name, WatchType2str[watch_item->type], pool, waitIfFull, paramList);
                        watch_item->pool = pool;
                        watch_item->waitIfFull = waitIfFull;
                        strncpy(watch_item->paramList, paramList, WATCH_PARAM_LEN);
                        result = 0;
                        break;
                }
            }
        }
    }
    PF_MutexRelease(Watch_Mutex);
    return result;
}


FPRESULT watch_getInfo(char *name,  WatchType_e type, int *found, int *pool, int *waitIfFull, char *paramList)
{
    Watch_Item *watch_item;
    FPRESULT result=0;

    if (pool == NULL || found == NULL || name == NULL || paramList == NULL) return FP_ERR_BADPARAM;
    *pool = WATCH_POOL_UNDEFINED;
    *found = _FALSE;
    PF_MutexHold(Watch_Mutex);
    if(!list_empty(&Watch_Head)){
        list_for_each_entry(watch_item, &Watch_Head, list) {
            if (watch_item->name != NULL && name != NULL) {
                if ((watch_item->pool != WATCH_POOL_UNDEFINED) &&
                    (watch_item->type == type) &&
                    (strstr(name, watch_item->name) != NULL)) {
                        TRACE_Log(TRACE_WATCH,"watch_check: Check found name [%s] type:[%s]\n",name,WatchType2str[type]);
                        strncpy(paramList, watch_item->paramList, WATCH_PARAM_LEN);
                        *pool = watch_item->pool;
                        *waitIfFull = watch_item->waitIfFull;
                        *found = _TRUE;
                        break;
                }
            }
        }
    }
    PF_MutexRelease(Watch_Mutex);
    return result;
}


void watch_dump(void)
{
    Watch_Item *watch_item;

    PF_MutexHold(Watch_Mutex);
    if(!list_empty(&Watch_Head)){
        list_for_each_entry(watch_item, &Watch_Head, list) {
            if (watch_item->name != NULL) {
                LOG_Log(LOG_PRINT,"watch_dump: name:[%s] type:%s pool:%d waitIfFull:%d paramList [%s]\n",
                    watch_item->name, 
                    WatchType2str[watch_item->type], 
                    watch_item->pool, 
                    watch_item->waitIfFull, 
                    watch_item->paramList);
            }
        }
    }
    PF_MutexRelease(Watch_Mutex);
}
