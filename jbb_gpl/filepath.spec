
BuildRoot:     %{buildroot}
Summary:       IBM Tivoli Storage Manager Client
License:       (c) International Business Machines Corp. 2011
Name:          TIVsm
Version:       %VERSION
Release:       %RELEASE
Group:         Utilities/Archiving
Vendor:        IBM
%description
This is the IBM Tivoli Storage Manager Client for Linux

%package       filepath
Group:         Utilities/Archiving
Summary:       the Filepath Module for %DISTRO
Vendor:        IBM
#Requires:      %REQUIRES

%description   filepath
IBM Tivoli Storage Manager Filepath kernel module required for Journal Based Backup.

%files filepath
%defattr(-,root,root)
%dir /opt/filepath
%attr(600,root,root) /opt/filepath/filepath.ko
%attr(700,root,root) /opt/filepath/filepath

%pre filepath
   if [ "$1" = "2" ]  # Upgrade
   then
      #--- Stop filepath so we can install new files
      /opt/filepath/filepath stop
   fi

   exit 0

%post filepath
   #--- Hook-in into the system boot stuff
   ln -sf /opt/filepath/filepath /etc/init.d/filepath

   /sbin/chkconfig --add filepath >/dev/null 2>&1

   #--- Start Filepath up.
   /opt/filepath/filepath start || echo ERROR: Failed to start filepath with error $?

   exit 0

%preun filepath
   if [ "$1" = "0" ]  # Complete uninstall
   then
      /opt/filepath/filepath stop >/dev/null 2>&1

      /sbin/chkconfig --del filepath >/dev/null 2>&1
      rm -f /etc/init.d/filepath
   fi

   exit 0

%postun filepath
   exit 0


