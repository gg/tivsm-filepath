/* log.h -- logging definitions
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef __LOG_H
#define __LOG_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>
#include "jbb.h"

    void LOG_LogDo(fpErrorLevel_e code, int line, char *file, char *format, ...);
    void LOG_Logv(fpErrorLevel_e code, char *preamble, char *format, va_list ap, int line, char *file);
    void LOG_SetTarget(PF_LogTarget target); /* Hierarchy */
    int LOG_SetTargetByStr(char *target);
    void LOG_GetTarget(PF_LogTarget *targ);
    extern void (*fplog_errStat)(fpErrorLevel_e code);
    extern int log_nerrors;

#define LOG_Log(code, format, ...) { \
    LOG_LogDo(code, __LINE__, __FILE__, format, __VA_ARGS__); \
    }

    /* Use this for internally detected error conditions that
    are survivable. NOTE: If the condition is at all anticipated
    it should instead be an FPERR_Announce() flavor so that it
    gets proper translation and can be masked and tracked.
    */
#define LOG_INFO  fpErrorLevel_Info_e
#define LOG_ERROR fpErrorLevel_Error_e

    /* Use this for internally detected error conditions that
    are NOT survivable and NOT expected to continue */
#define LOG_PANIC fpErrorLevel_Panic_e

#define LOG_PRINT fpErrorLevel_Print_e

#ifdef __cplusplus
}
#endif

#endif
