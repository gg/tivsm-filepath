/* error.c -- Error handling
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



Please note, this is separate from "log" and basically
a logically layer on top. Errors can be individually 
turned off. Then general "fperr_Announce()" functions 
knows how/if to display an error (and ultimately
calls the logging functions to do it).

This is completely separate from tracing. 

*/

#include "jbb.h"
/*
* Define the macro-expanders to create a an array of
* structs. Each struct will have an error's definition which 
* will include the error value, the mnenomic, the severity
* level, the translations-phrase, and a flag for disabling
* it and count for how many times this error has occured.
*/
struct errdef {
    int val; 
    char *codestr; 
    int severity; 
    char *msg, *orig; 
    int disabled; 
    int times;
    int msgAllocated; /* mem_alloc'd or static */
};


enum {FP_PANIC=fpErrorLevel_Panic_e,
    FP_INFO=fpErrorLevel_Info_e,
    FP_ERR=fpErrorLevel_Error_e,
    FP_WARN=fpErrorLevel_Warn_e};

#define FP_ERR(n, lvl, shr, msg) \
{n, #shr, lvl, msg, 0, 0, 0 },


static struct errdef errs[]={
    FP_ERRs
    {FP_ERR_BAD, "WEIRD", FP_ERR, "WEIRD ERROR", 0, 0, 0}
};


/*
* Given an error value, find the index into the 
* error array.
*/
static unsigned int fperr2idx(int err) {
    unsigned int i;
    for (i=0; i<sizeof(errs)/sizeof(errs[0]); i++)
        if (err==errs[i].val) return i;
    return i-1;  /* End of list */
}



fpErrorLevel_e FPERR_getSeverity(int err) {
    int i=fperr2idx(err);
    return errs[i].severity;
}

char *FPERR_err2mnemonic(int err) {
    int i=fperr2idx(err);
    return errs[i].codestr;
}

static FPRESULT convertVarToPercents(char *msg, int max) {
    STR_substitute(msg, max, "<var type=\"string\"/>","%s");
    STR_substitute(msg, max, "<var type=\"int32\"/>","%d");
    STR_substitute(msg, max, "<var type=\"int64\"/>",PF_64d);
    if (strstr(msg,"<var")) {
        LOG_Log(LOG_ERROR,"fperror.c convert failed:%s\n",msg);
        return FP_ERR_UNKNOWNVAR;
    }
    return 0;
}


/*
* Cleanup handler; return all allocated strings.
*/
void error_fini(void) {
    unsigned int i;
    for (i=0; i<sizeof(errs)/sizeof(errs[0]); i++) {
        if (errs[i].msgAllocated) {
            errs[i].msgAllocated=0;
            MEM_free(errs[i].msg);
            if (errs[i].orig)
                errs[i].msg=errs[i].orig;
            else 
                errs[i].msg="(msg deleted)";
        }
    }
}


static void convertVarToPercents_all(void) {
    unsigned int i;
    static int first_time=1;

    if (!first_time) return;
    first_time=0;

    for (i=0; i<sizeof(errs)/sizeof(errs[0]); i++) {
        if (!errs[i].msgAllocated && strstr(errs[i].msg,"<var ")) {
            // Must have non-static-strings for STR_substitute
            char *p=errs[i].msg;
            errs[i].msg=MEM_malloc(-8,strlen(p)+1,"er11");
            strncpy(errs[i].msg,p,strlen(p)+1);
            errs[i].msgAllocated = 1;
        }
        convertVarToPercents(errs[i].msg,strlen(errs[i].msg)+1);
    }
}

/*
* Given an error value, translate it to the appropriate
* message phrase. NORMALLY a caller should call the Announce
* function. This one is mostly for GUI translation of
* words or phrases (rather than for errors).
*/
char *FPERR_err2str(int err) {
    int i=fperr2idx(err);
    /* What about if it is disabled? Should we return
    null?  or ""? or the real message? */
    convertVarToPercents_all();
    return errs[i].msg;
}


/* Basically, determine if the string ends with a carriage return */
static char lastChar(char *str) {
    int i;
    i=strlen(str);
    while (i>1) {
        i--;
        if (str[i]>' ') return str[i];
        if (str[i]=='\n') return str[i];
    }
    return str[i];
}


/*
* This is the main function of this module. The caller
* has encountered some condition that probably needs someone's
* attention.  By calling this function, we centrally can
* print/log the condition as well as centrally translate the
* error's phrase and centrally disable an error from being
* reported entirely.
*/
void FPERR_Announce(int err,...) {
    int i=fperr2idx(err);
    va_list ap; 
    char temp[512],*p, preamble[40];

    convertVarToPercents_all();
    if (errs[i].disabled) return;

    snprintf(preamble, sizeof(preamble), "%s(%d):", errs[i].codestr, err);
    va_start(ap, err);  
    /* Attempt to deal with some of the error strings not ending with
    a carriage return.  Some wont, because they weren't intended for
    print-out as a fullly formatted string (but rather as a partial),
    yet, on some occasions, we may be signalled to print it out as
    a full message. */
    p=errs[i].msg;
    //  if (!strchr(p,'\n') && strlen(p)<sizeof(temp)-1) {
    if (lastChar(p)!='\n' && strlen(p)<sizeof(temp)-1) {
        strncpy(temp, p, sizeof(temp));
        temp[sizeof(temp)-10]=0; // z
        strcat(temp,"\n");
        p=temp;
    }
    LOG_Logv(errs[i].severity, preamble, p, ap, -1, "");
    va_end(ap);
    errs[i].times++;
}





