/* log.c -- log message handling.  Writes all messages to specified location.
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "jbb.h"

static char *fpErrorLevel2str[]={
    "PANIC","ERROR","Warn","Info","Print"};

    static PF_LogTarget target = PF_SysLog;

    int log_nerrors;
    void (*fplog_errStat)(fpErrorLevel_e code);

    void LOG_Logv(fpErrorLevel_e code, char *preamble, char *format, va_list ap, int line, char *file) {
        static char last[FPLOG_BUF_SIZE];
        static int lastCount;
        static int active;
        static char buff[FPLOG_BUF_SIZE];  /* NOT reentrant...small kernel stacks */
        static char msg[FPLOG_BUF_SIZE+sizeof(PF64LONG)];
        char temp[100];

        /* First, allow other layers to know about this if this is an
        error. Mostly, allow a user-mode app to inject an err count
        into the kernel. */
        if (code <= fpErrorLevel_Error_e && !active) {
            log_nerrors++;
            active=1;
            if (fplog_errStat) fplog_errStat(code);
            active=0;
        }
        if (code < 0 || code >= sizeof(fpErrorLevel2str)/sizeof(fpErrorLevel2str[0]))
            code= fpErrorLevel_Error_e;

        msg[0]=0;
        buff[0]=0;

        /* Put the "preface" on the output line if not in trace mode */
        if (code != fpErrorLevel_Print_e) {
            char *filename = NULL;
            char loc[1024];
            loc[0] = 0;
            if (line != -1) {
                if (file) filename = strrchr(file,'/');
                if (filename) snprintf(loc, 1024, "[%-7s %3d]", filename+1, line);
            }
            snprintf(buff,sizeof(buff),"%s TsmJbb %s %s",loc,fpErrorLevel2str[code],preamble);
        }

        vsnprintf(&buff[strlen(buff)],sizeof(buff)-10,format,ap);  
        buff[sizeof(buff)-1]=0;  /* Ensure z-byte */

        /*
        * Collapse duplicate messages...very important in case
        * there is some run-away error condition.
        */
        if (last[0] && strcmp(buff, last)==0) lastCount++;
        else {
            if (lastCount) {
                snprintf(temp,sizeof(temp)-2,FPERR_err2str(FP_INFO_MSGREPEAT),lastCount);
                strcat(temp,"\n");
                PF_Log(target, temp);
            }    
            // Need to tack on the time Feature 309 - looks like AIX is the only one that does
            // not use the system log, so add a time stamp to it before writing it out.
            PF_Log(target, buff);
            strncpy(last, buff, sizeof(last));
            lastCount=0;
        }
    }


    void LOG_LogDo(fpErrorLevel_e code, int line, char *file, char *format, ...) {
        va_list ap; 
        va_start(ap, format);  
        LOG_Logv(code, "", format, ap, line, file);
        va_end(ap);
    }

    void LOG_SetTarget(PF_LogTarget targ) {
        if ((int) targ != -1) target = targ;
    }

    void LOG_GetTarget(PF_LogTarget *targ) {
        if (targ) *targ = target;
    }


    int LOG_SetTargetByStr(char *target) {
        PF_LogTarget targ;

        if (!target || !target[0]) targ=(PF_LogTarget)-1;
        else
            if ((STR_strcasecmp(target,"screen")==0)||(STR_strcasecmp(target,"terminal")==0))
                targ=PF_Terminal;
            else 
                if (STR_strcasecmp(target,"file")==0) targ=PF_SysLog;
                else return FP_ERR_IVLOGDEV;
                LOG_SetTarget(targ);
                return 0;
    }

