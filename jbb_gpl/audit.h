/* audit.h -- Audit / Buffering definitions
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/

#ifndef __AUDIT_H
#define __AUDIT_H

#ifdef __cplusplus
extern "C" {
#endif

/*=== 256 MB used in audit_setMaxSize ===*/
#define AUDIT_UPPER_LIMIT 256 
/*=== Default limit. Change with fpa command <buffer-max size=x/> x in MB ===*/
#define MAX_AUDIT_BUFFER_SIZE (1024*1024*64) 
/*=== Defualt size ===*/
#define MIN_AUDIT_BUFFER_SIZE (1024*1024*1)
/*=== How much to extend if buffer becomes full ===*/
#define GROW_AUDIT_BUFFER_SIZE (1024*1024*4)
/*=== Set the internal buffer to X times the Notify Buffer ===*/
#define AUDIT_BUFFER_MULTIPLE 1 

    FPRESULT audit_init(int maxBufferSize, int allowWaitIfFull); 
    void audit_fini(void);
    FPRESULT audit_logToPool(char *filename, char *msg, int pool, PF_Bool waitIfFull);
    FPRESULT audit_drainPool(char *buf, int max, int pool, PF32LONG *moved);
    FPRESULT audit_sizePool(int size, int pool, int multiply);
    FPRESULT audit_addPool(int pool);
    void audit_removePool(int pool);
    void audit_attachParty(int pool);
    FPRESULT audit_setMaxSize(int size);

#ifdef __cplusplus
}
#endif


#endif
