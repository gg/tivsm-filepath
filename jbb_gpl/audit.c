/* audit.c -- Audit functions
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


__________________________________________________________________________________

AUDIT.C

Controls multiple audit buffers that are held in a linked list Audit_Item structures.

audit_addPool  - Called from parse_action in jbb.c - sets up a new pool.
audit_removePool - Called from parse_rule_del to delete an existing pool.

audit_logToPool - Called by jbb_doLog to add a record to an audit buffer pool.
audit_sizePool  - Called from jbb.c parse_audit_size to set the size of a buffer pool.
audit_drainPool - Called from jbb.c parse_audit_get to copy to contents of a pool into a buffer.

audit_attachParty - Called form jbb.c parse_attachParty Starts a process monitor located in pf.c

*/

#include <linux/list.h>

#include "jbb.h"

extern jbbStats g_jbbStats;

#define PIECE_START "<audit LogToFile=\""
#define PIECE_MIDDLE "\" LogParam=\'"
#define PIECE_END "\'/>"
#define OVERHEAD (strlen(PIECE_START)+strlen(PIECE_MIDDLE)+strlen(PIECE_END))

typedef struct _Audit_Item {
    struct list_head  list;
    char              ifFullSem[PF_SEM_SIZE];
    char             *buffer, *next, *end;
    int               bufferSize, full, timesDown, overFlow;
    int               pool;  // pool name basically
} Audit_Item;

static LIST_HEAD(Audit_Head);
static char *Audit_Mutex = NULL;
static int g_auditBufferMax = MAX_AUDIT_BUFFER_SIZE;
static int g_auditAllowWaitIfFull = 1;

// Called from module_init in hook.c
FPRESULT audit_init(int maxBufferSize, int allowWaitIfFull) 
{
    FPRESULT result = 0;

    g_auditBufferMax = maxBufferSize *1024 * 1024;
    if (maxBufferSize != MAX_AUDIT_BUFFER_SIZE) {
       LOG_Log(LOG_INFO,"Audit_Init: Max Buffer now: %d\n",g_auditBufferMax);
    }
    g_auditAllowWaitIfFull = allowWaitIfFull;
    LOG_Log(LOG_INFO,"Audit_Init: Allow buffer wait set to %s\n",g_auditAllowWaitIfFull ? "TRUE" : "FALSE");
    if (Audit_Mutex) return result;
    Audit_Mutex=MEM_malloc(-8,PF_SEM_SIZE,"aumu");
    if (!Audit_Mutex) return FP_ERR_NOMEM;
    result=PF_MutexInit(PF_SemSystem, Audit_Mutex);
    if (result) {
        LOG_Log(LOG_ERROR,"Audit_Init: failed mutex init:%d\n",result);
        return result;
    }
    return result;
}


void audit_fini(void) 
{
    Audit_Item *audit_item = NULL, *next = NULL;

    if (Audit_Mutex) {
        PF_MutexHold(Audit_Mutex);
        if(!list_empty(&Audit_Head)){
            list_for_each_entry_safe(audit_item, next, &Audit_Head, list) {
                PF_SemDestroy(audit_item->ifFullSem);
                MEM_free(audit_item->buffer);
                list_del(&audit_item->list);
                MEM_free(audit_item);
            }
        }
        PF_MutexRelease(Audit_Mutex);
        PF_MutexDestroy(Audit_Mutex);
        MEM_free(Audit_Mutex);
        Audit_Mutex = NULL;
    }
}


FPRESULT audit_addPool(int pool)
{
    FPRESULT result=0;
    Audit_Item *audit_item = NULL;
    int found = _FALSE;

    TRACE_Log(TRACE_AUDIT,"audit_addPool: Adding pool %d\n",pool);
    do {
        PF_MutexHold(Audit_Mutex);
        if(!list_empty(&Audit_Head)){
            list_for_each_entry(audit_item, &Audit_Head, list) {
                if (audit_item->pool == pool) {
                    found = _TRUE;
                    break;  // Already exists
                }
            }
        }
        if (found == _FALSE) {
            audit_item = MEM_malloc(-8,sizeof(*audit_item),"apol");
            if (!audit_item) result = FP_ERR_NOMEM;
            else {
                PF_SemInit(PF_SemSystem, audit_item->ifFullSem, 1);
                audit_item->buffer = MEM_malloc(-8,MIN_AUDIT_BUFFER_SIZE,"audt");
                audit_item->bufferSize = MIN_AUDIT_BUFFER_SIZE;
                if (!audit_item->buffer) {
                    LOG_Log(LOG_ERROR,"audit_addPool: pool %d Fail to allocate audit buffer, size:%d\n",pool,MIN_AUDIT_BUFFER_SIZE);
                    result = FP_ERR_NOMEM;
                    break; // Exit while
                }
                audit_item->next = audit_item->buffer;
                audit_item->buffer[0]=0;
                audit_item->end = &audit_item->buffer[MIN_AUDIT_BUFFER_SIZE];
                audit_item->pool = pool;
                audit_item->overFlow = 0;
                list_add(&audit_item->list, &Audit_Head);
            }
        }
    }
    while (0);
    PF_MutexRelease(Audit_Mutex);
    return result;
}


FPRESULT audit_setMaxSize(int size)
{
   int result = FP_INFO_SUCCESS;

   if (size <= AUDIT_UPPER_LIMIT) {
      g_auditBufferMax = size *1024 * 1024;
      LOG_Log(LOG_INFO,"audit_setMaxSize: Setting max buffer size to %d MB.\n",size);
   }
   else {
      LOG_Log(LOG_ERROR,"audit_setMaxSize: Attempting to set buffer max to %d MB limit is %d MB.\n",size,AUDIT_UPPER_LIMIT);
      LOG_Log(LOG_INFO,"audit_setMaxSize: Setting max buffer size to %d MB.\n",AUDIT_UPPER_LIMIT);
      g_auditBufferMax = AUDIT_UPPER_LIMIT *1024 * 1024;
      result = FP_ERR_BADPARAM;
   }
   return result;
}


static FPRESULT audit_growBuffer(Audit_Item *audit_item, int inSize)
{
   FPRESULT result=FP_INFO_SUCCESS;
   int size = 0;
   int offset = 0;
   char *buf = NULL;

   if (audit_item == NULL) 
      return FP_ERR_BADPARAM;

   if (inSize == 0) {
      size = audit_item->bufferSize + GROW_AUDIT_BUFFER_SIZE;
      if (size > g_auditBufferMax) 
         size = g_auditBufferMax;  
   }
   else {
      size = inSize; // coming from audit_sizePool
   }

   if (audit_item->bufferSize == g_auditBufferMax) {
      static PFTIME last_time=0;
      PFTIME now=pf_secs();
      result = FP_ERR_BADPARAM; 
      if (now - last_time > 10) {
         LOG_Log(LOG_ERROR,"audit_growBuffer: Attempting to set buffer to %d but the max is %d.\n",size,g_auditBufferMax);
         LOG_Log(LOG_INFO,"audit_growBuffer: Use fpa cmd <buffer-max size=num/> where num is in MBs. %d\n",result);
      }
      last_time = now;
   }
   else {   
      buf = MEM_malloc(8, size, "audb");
      if (!buf) result = FP_ERR_NOMEM;
      else {
         LOG_Log(LOG_INFO,"audit_sizePool: Setting pool %d buffer size to %d\n",audit_item->pool,size);
         // if there is an existing buffer, copy it, free it
         if (audit_item->buffer) {
            offset = audit_item->next - audit_item->buffer;
            memcpy(buf, audit_item->buffer, offset);
            MEM_free(audit_item->buffer);
         }
         audit_item->buffer = buf;
         audit_item->end = &audit_item->buffer[size];
         audit_item->next = &audit_item->buffer[offset];
         *audit_item->next = 0;
         audit_item->bufferSize = size;
      }
   }
   return result;
}
 
/* 
*  There are 2 ways to set the size:
*    If multiply is set to 0 then set the size to inSize
*    If multiply is set to 1 then set the size to AUDIT_BUFFER_MULTIPLE
*      times the inSize.
*    Of course check to see if inSize is within reason
*/
FPRESULT audit_sizePool(int inSize, int pool, int multiply)
{
    FPRESULT result=0;
    Audit_Item *audit_item = NULL;
    int size = 0;
    int multiple = AUDIT_BUFFER_MULTIPLE;

    if (multiply == 0) { // comes from <buffer-size size=bytes/> command
       multiple = 1;
    }
    /*===  Set buffer to X (audit.h) time the user buffer size ===*/
    if ((inSize * multiple) <= MIN_AUDIT_BUFFER_SIZE) {
       return result; // Default is larger then asked for... keep the default
    }
    else if ((inSize * multiple) >= g_auditBufferMax) {
       size = g_auditBufferMax;
    }
    else {
       size = inSize * multiple;
    }

    PF_MutexHold(Audit_Mutex);
    if(!list_empty(&Audit_Head)){
        list_for_each_entry(audit_item, &Audit_Head, list) {
            if (audit_item->pool == pool) {
                // Do not allow the size of the buffer to shrink
                if (audit_item && audit_item->buffer && audit_item->bufferSize > size && size != 0) {
                    LOG_Log(LOG_ERROR,"audit_sizePool: Shrinking the buffer is not supported. current:% request:%d\n",audit_item->bufferSize, size);
                    result = FP_ERR_BADPARAM;
                }
                else {
                    // changing the current buffer size        
                    result = audit_growBuffer(audit_item, size);
                }
                break;
            }
            else {
               result = FP_ERR_BADPARAM; // Pool Not Found
            }
        }
    }
    PF_MutexRelease(Audit_Mutex);
    return result;
}


FPRESULT audit_logToPool(char *filename, char *msg, int pool, PF_Bool waitIfFull) {
    FPRESULT result=0;
    Audit_Item *audit_item = NULL;
    int n,n2;

    FPENTER(TRACE_AUDIT,"audit_logPool","pool %d filename:%s msg:%s\n",pool,filename,msg);
    if (filename == NULL || msg == NULL) return FP_ERR_BADPARAM;
retry:
    PF_MutexHold(Audit_Mutex);
    if(!list_empty(&Audit_Head)){
        list_for_each_entry(audit_item, &Audit_Head, list) {
            if (audit_item->pool == pool) {
                n=strlen(msg);
                n2=strlen(filename);
                
                if (audit_item->next + n + n2 + OVERHEAD + 5 /*safety*/ > audit_item->end) {
                   result = audit_growBuffer(audit_item, 0);  // 0 means grow by default step (audit.h)
                }

                if (result != 0) {
                    static PFTIME last_time=0;
                    PFTIME now=pf_secs();

                    if (waitIfFull && g_auditAllowWaitIfFull) {
                       result = 0;  // reset for retry
                       audit_item->full=1;
                       g_jbbStats.timesAuditBlocked++;
                       audit_item->timesDown++;
                       if (now - last_time > 10) {
                          LOG_Log(LOG_INFO,"audit_logPool: full, blocking on waitIfFull. pool:%d  %d\n",pool,audit_item->timesDown);
                       }
                       PF_MutexRelease(Audit_Mutex);
                       PF_SemDown(audit_item->ifFullSem, 0); // Cleared in audit_drainPool
                       goto retry;  // Go back and try again
                    }
                    else {
                       result = FP_ERR_AUDITOVERFLOW;
                       if (audit_item->overFlow == -1) { // Has it been sent yet in audit_drainPool?
                          audit_item->overFlow = 0;
                       } 
                       else {
                          audit_item->overFlow = 1;
                       }
                       g_jbbStats.auditOverflows++;
                       if (now - last_time > 30) {
                          FPERR_Announce(FP_ERR_AUDITOVERFLOW);
                       }
                    }
                    last_time = now;
                }
                else {
                    strcpy(audit_item->next, PIECE_START); audit_item->next+=strlen(PIECE_START);
                    strcpy(audit_item->next, filename); audit_item->next+=n2;
                    strcpy(audit_item->next, PIECE_MIDDLE); audit_item->next+=strlen(PIECE_MIDDLE);
                    strcpy(audit_item->next, msg); audit_item->next+=n;
                    strcpy(audit_item->next, PIECE_END); audit_item->next+=strlen(PIECE_END);
                    g_jbbStats.naudits++;
                    g_jbbStats.auditdepth= audit_item->next - audit_item->buffer;
                }
                break;
            }
        }
    }
    PF_MutexRelease(Audit_Mutex);
    FPEXIT(TRACE_AUDIT,"audit_logPool",result);
}


FPRESULT audit_drainPool(char *buf, int max, int pool, PF32LONG *moved) {
    FPRESULT result=0;
    Audit_Item *audit_item = NULL;
    int tomove, leftover=0;
    int trace=TRACE_DEBUG1;

    if (buf == NULL || moved == NULL) return FP_ERR_BADPARAM;
    TRACE_Log(TRACE_DEBUG1,"audit_drainPool: buf:%p pool:%d max:%d\n",buf, pool, max);
    PF_MutexHold(Audit_Mutex);
    if(!list_empty(&Audit_Head)){
        list_for_each_entry(audit_item, &Audit_Head, list) {
            if (audit_item->pool == pool) {
                if (audit_item->overFlow == 1) {
                  //LOG_Log(LOG_INFO,"audit_drainPool: Setting pool %d ovf:%d\n",audit_item->pool,audit_item->overFlow);
                  result = FP_ERR_AUDITOVERFLOW;  // Buffer has overflowed in the past - game over
                  audit_item->overFlow = -1; // Set for reset when logged to the next time by audit_logToPool
                  audit_item->buffer[0]=0;   // Flush the buffer
                  audit_item->next = audit_item->buffer;
                }
                else {
                   result = 0;
                   if (audit_item->next != audit_item->buffer) trace=TRACE_AUDIT;
                   tomove = audit_item->next - audit_item->buffer;
                   /* If the buffer isn't big enough, then move as many complete
                   messages as possible. So, from the end, look backwards until
                   either the the end/start of a message or beg-of-buff. */
                   if (tomove >= max) {
                      char *p1=audit_item->buffer+max-1;
                      while (p1>audit_item->buffer) {
                         if (p1[0]=='<' && p1>audit_item->buffer+3 &&
                            p1[-1]=='>' &&
                            p1[-2]=='/') {
                               tomove = p1 - audit_item->buffer;
                               leftover = audit_item->next - p1;
                               break;
                         }
                         else p1--;
                      }
                   }

                   *moved = 0; 
                   if (tomove) {
                      *moved = tomove + 1;  // z-byte
                      memcpy(buf, audit_item->buffer, tomove);
                      buf[tomove]=0;        // force the z

                      if (leftover) {
                         /*=== Replace memcpy with memmove defect 87700 ===*/
                         memmove(audit_item->buffer, &audit_item->buffer[tomove], leftover);
                      }
                      audit_item->next = &audit_item->buffer[leftover];
                      audit_item->buffer[leftover]=0;
                      g_jbbStats.auditdepth = leftover;  
                      if (audit_item->full && leftover < (audit_item->bufferSize/2)) {
                         int i,times=audit_item->timesDown;
                         audit_item->timesDown=0;
                         audit_item->full=0;  // clear this before we allow the waiter(s) to charge ahead...
                         for (i=0; i<times; i++) {
                            PF_SemUp(audit_item->ifFullSem);
                            TRACE_Log(TRACE_DEBUG1,"audit_logPool: full, unblocking pool:%d  %d\n",pool,i);
                         }
                      }
                      if (leftover > 0) {
                        LOG_Log(LOG_INFO,"audit_drainPool: pool:%d drained:%d leftover:%d size:%d max:%d\n",
                           pool, *moved, leftover,audit_item->bufferSize,g_auditBufferMax);
                      }
                      TRACE_Log(TRACE_AUDITOUT,"audit_drainPool: pool:%d moved:%d leftover:%d size:%d max:%d\n",
                         pool, *moved, leftover,audit_item->bufferSize,g_auditBufferMax);
                   }
                }
                break;
            }
            else {
               result = FP_ERR_BADPARAM; // Pool Not Found
            }
        }
    }
    PF_MutexRelease(Audit_Mutex);
    TRACE_Log(trace,"audit_drainPool: moved:%d\n", *moved);
    return result;
}


void audit_removePool(int pool) 
{
    Audit_Item *audit_item = NULL;

    PF_MutexHold(Audit_Mutex);
    if(!list_empty(&Audit_Head)){
        list_for_each_entry(audit_item, &Audit_Head, list) {
            if (audit_item->pool == pool) {
                PF_StopMonitor(pool);
                PF_SemDestroy(audit_item->ifFullSem);
                MEM_free(audit_item->buffer);
                list_del(&audit_item->list);
                MEM_free(audit_item);
                audit_item = NULL;
                break;
            }
        }
    }
    PF_MutexRelease(Audit_Mutex);
}

/******************* Monitor Attached Processes ********************/

void audit_attachParty(int pool)
{
    PF_StartMonitor(pool);
}


