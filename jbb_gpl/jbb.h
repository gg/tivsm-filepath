/* jbb.h -- Main facility holding point.  All files must include this header.
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/

#ifndef __FPR_H
#define __FPR_H
#ifdef __cplusplus
extern "C" {
#endif

#include <linux/kernel.h> /* snprintf */
#include <linux/string.h> /* string routines except strncasecmp for 2.6.17 and below */
#include <linux/limits.h> /* #define PATH_MAX  4096 */

#include "error.h"
#include "pf.h"
#include "log.h"
#include "mem.h"
#include "trace.h"
#include "hash.h"
#include "str_supp.h"
#include "xml.h"
#include "watch.h"
#include "ir.h"
#include "audit.h"
#include "jbb_version.h"

#define _FALSE 0
#define _TRUE  1
#define NOT_DIRTY     0
#define DIRTY_DATA    1 /*=== "dirty" file data by create() / write() / truncate() ===*/
#define DIRTY_ATTR    2 /*=== "dirty" file attributes by chown() / chmod() ===*/

#define PF_IOCTL_CTL      100  // The ioctl data is a fpaCtl one
#define PF_IOCTL_HOOK     101
#define PF_IOCTL_HOOK_DIR 102

    typedef struct {
        /* input */
        PF32LONG dataStructureVersion;  /* to allow for future changes and compatibility */
        PF32LONG subCommand;
        PF32LONG _thisSize;             /* Size of this overall struct; not used any more */
        PF32LONG nBytesIn;              /* n bytes valid for input at data */
        PF32LONG maxOut;                /* Allocate size at 'data' */
        PF32LONG hpMajor;               /* HP only; this is what the ioctl num *would* be */
        /* output */
        PF32LONG nBytesOut;             /* n bytes written into 'data' */
        PF32LONG pad;                   /* Align on 8-byte */
        PF32LONG future[4];             /* Future use */
        FPRESULT result;                /* FP Result code for operation */
        PF32LONG pad2;                   /* Align on 8-byte */
        char * dataptr;                  /* points to the data field  */  
        PF32LONG filler[4];             /* Future use */
    } PF_Ctl_t;


    typedef enum {  
        fpaCtl_XML_e         /* In/out   */  /* XML both directions */
    } fpaCtl_e;

#define DIRSLASH '/'
#define SYSLOG_FILE_BASE "syslog"
#define SYSLOG_FILE "/var/log/messages"
#define JBB_DEFAULT_EXCLUDE_LIST "/usr/lib/,/usr/lib64/,/lib,/bin/,/sbin/,/etc/,/usr/share/,/usr/bin/,/dev/,/var/,/sys/,/boot/,/proc/"
    //#define JBB_DEFAULT_EXCLUDE_LIST "/usr/lib/,/usr/lib64/,/lib,/bin/,/sbin/,/usr/bin/,/dev/,/boot/,/proc/"

    typedef struct {
        PF32LONG naudits;
        PF32LONG nerrors;
        PF32LONG auditOverflows;
        PF32LONG timesAuditBlocked;
        PF32LONG auditdepth;
        PF32LONG mem_inuse;
        PF32LONG infoRecDepth;
        PF32LONG watchDepth;
    } jbbStats;

    extern jbbStats g_jbbStats;

#define ALL_STATS(_one,_two) \
    STAT(_one, _two, "nerrors", stats.nerrors);		    \
    STAT(_one, _two, "auditOverflows", stats.auditOverflows);	  \
    STAT(_one, _two, "timesAuditBlocked", stats.timesAuditBlocked);	\
    STAT(_one, _two, "auditdepth", stats.auditdepth);			\
    STAT(_one, _two, "mem_inuse", stats.mem_inuse);			\
    STAT(_one, _two, "naudits", stats.naudits);				\
    STAT(_one, _two, "infoRecDepth", stats.infoRecDepth);			\
    STAT(_one, _two, "watchDepth", stats.watchDepth);			\


    // If a value is added to the enum below you must add a string value to VfsAction2str in jbb.c 
    typedef enum {
        VfsOpen_e,
        VfsUnlink_e, 
        VfsMkdir_e, 
        VfsReduceSize_e,
        VfsRmdir_e, 
        VfsRename_e, 
        VfsChattr_e,
        VfsRead_e,
        VfsWrite_e,
        VfsClose_e, 
        VfsChDate_e,
        VfsMknod_e,
        VfsLink_e,
        VfsSymlink_e 
    } VfsAction_e;


    /* Functions in jbb.c */
    int jbb_log_data(char *filename, char *toFilename, VfsAction_e vfsAction, PFTIME mtime, WatchType_e type);
    FPRESULT jbb_ctl(PF_Ctl_t *c, char *data);
    char *jbb_getExcludeList(void);
    char *jbb_getWatchList(void);
    PF_Bool jbb_checkExcludeList(char *filename);
    PF_Bool jbb_checkWatchList(char *filename, WatchType_e type);

#ifdef __cplusplus
}
#endif


#endif
