/* error.h -- Error code definitions and handling
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef __ERROR_H
#define __ERROR_H

#ifdef __cplusplus
extern "C" {
#endif
#include "jbb.h"

    /*
    * First, define 5 error "levels". Every error defined
    * will be given a level as well.
    */
    typedef enum {fpErrorLevel_Panic_e, 
        fpErrorLevel_Error_e,
        fpErrorLevel_Warn_e,
        fpErrorLevel_Info_e,
        fpErrorLevel_Print_e
    } fpErrorLevel_e;

    /* Now define errors */
#define FP_ERRs \
    FP_ERR(0,FP_INFO,SUCCESS,"Success") \
    FP_ERR(5001,FP_ERR,NOMEM,"No memory available for operation") \
    FP_ERR(5002,FP_ERR,INTRPTD,"Semaphore interrupted") \
    FP_ERR(5004,FP_ERR,BAD_IOCTL,"Unknown IOCTL value") \
    FP_ERR(5005,FP_ERR,DUPLICATE_WATCH,"Duplicate rule or watch found.") \
    FP_ERR(5006,FP_ERR,NO_WATCH_MATCH,"Could not find a watch to match the pool number.") \
    FP_ERR(5010,FP_ERR,IVLOGLVL,"The specified tracing level is not known.") \
    FP_ERR(5011,FP_ERR,IVLOGDEV,"The specified logging device is unknown or unsupported on this platform.") \
    FP_ERR(5012,FP_ERR,OVERFLOW,"Some internal buffer overflowed") \
    FP_ERR(5022,FP_ERR,BADFILE,"Unable to access the specified file") \
    FP_ERR(5026,FP_ERR,TOOSMALL,"The 'audit buffer' is not big enough to hold any more messages.") \
    FP_ERR(5047,FP_ERR,SYSERR,"A system command (such as mkdir or unlink or system) resulted in an error") \
    FP_ERR(5049,FP_INFO,XMLRESPONSE,"Result is in XML; this is not an error") \
    FP_ERR(5050,FP_ERR,HOOKFILE,"Failed to open hook file") \
    FP_ERR(5044,FP_ERR,NULLPARAM,"Null parameter encountered") \
    FP_ERR(5054,FP_ERR,BADPARAM,"A parameter specified does not meet the contract of the function") \
    FP_ERR(5055,FP_ERR,HASHDUP,"Duplicate value in hash") \
    FP_ERR(5056,FP_ERR,IVVALUE,"Value specified for an error value is either missing or invalid") \
    FP_ERR(5057,FP_ERR,IVSYNTAX,"The xml paragraph does not contain the 'MsgText' and '/MsgText' tags") \
    FP_ERR(5058,FP_ERR,HASHNULL,"A hash is trying to be accessed but it hasn't been set up yet.") \
    FP_ERR(5061,FP_ERR,UNKNOWNVAR,"There is an &lt; var &gt; item in a message that is not understood or is invalid.") \
    FP_ERR(5076,FP_ERR,IVVFSTYPE,"The vfs specified for the \"inject-file\" message is not known.") \
    FP_ERR(6001,FP_INFO,MSGREPEAT,"Last message repeated <var type=\"int32\"/> times.") \
    FP_ERR(6010,FP_ERR,ALLOC,"Memory allocation failed of <var type=\"int32\"/> bytes in function <var type=\"string\"/>.") \
    FP_ERR(6011,FP_ERR,BADIOCTL,"Unknown/unsupported IOCTL value <var type=\"int32\"/> was given.") \
    FP_ERR(6024,FP_ERR,HOOKIVF,"Invalid file (or no file) specified for the HOOK/HOOKDIR operations.") \
    FP_ERR(6025,FP_ERR,IOCTLFAIL,"Ioctl <var type=\"int32\"/> failed with error (<var type=\"int32\"/>) <var type=\"string\"/>.") \
    FP_ERR(6026,FP_ERR,OSINTERNAL,"During function '<var type=\"string\"/>', a call to OS function '<var type=\"string\"/>' for file '<var type=\"string\"/>' resulted in error <var type=\"int32\"/>.") \
    FP_ERR(6030,FP_ERR,AUDITOVERFLOW,"The kernel 'audit buffer' overflowed; some audits are now lost.") \
    FP_ERR(6033,FP_INFO,READY,"TSMJBB driver loaded and ready.") \
    FP_ERR(6035,FP_INFO,UNLOADING,"TSMJBB driver unloading.") \
    FP_ERR(6036,FP_INFO,BADXML,"The system could not recognize the XML command: <var type=\"string\"/>.") \
    FP_ERR(6089,FP_INFO,MISCERR,"TSMJBB has experienced a problem; Please see the system event log for details.") \


    /* Next, create an enum that lists-out all the error IDs */

    enum FP_ErrEnum {
#define FP_ERR(code, severity, shortStr, longStr) \
    severity##_##shortStr = code,
        FP_ERRs
        FP_ERR_Last };
#undef FP_ERR

#define FP_ERR_BAD FP_ERR_Last

        void error_fini(void);
        void FPERR_Announce(int err,...); /* Main function...announce
                                          an error condition to the
                                          logger module...translated */
        char *FPERR_err2str(int err);  /* Get ascii msg translation */
        char *FPERR_err2mnemonic(int err); /* Get ascii string of error 'code' */
        fpErrorLevel_e FPERR_getSeverity(int err);

#ifdef __cplusplus
}
#endif

#endif
