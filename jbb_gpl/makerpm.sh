#!/bin/sh

RELNUM=$1
WORKDIR=$PWD/rpmbuild

BUILDROOT=$WORKDIR/buildroot
mkdir -p $BUILDROOT/opt/filepath

# copy files to RPM buildroot
cp filepath.ko filepath $BUILDROOT/opt/filepath

# get version and release from RELNUM
RPMVERSION=`echo $RELNUM | sed 's/\.[0-9]*$//'`
RPMRELEASE=`echo $RELNUM | sed 's/^.*\.//'`

# get the Linux description
DISTRO=`lsb_release -ds`

# build the RPM!
rpmbuild --define "VERSION $RPMVERSION" --define "RELEASE $RPMRELEASE" --define "DISTRO $DISTRO" -bb filepath.spec --buildroot $BUILDROOT > rpmbuild.out 2>&1

# did we get anything?
RPMNAME=`egrep 'TIVsm-filepath.*\.rpm' rpmbuild.out | sed 's/\s*$//' | sed 's/.* //'`

if [ -z "$RPMNAME" ]
then
  echo "*** rpmbuild output ***"
  cat rpmbuild.out
  rm -rf $WORKDIR rpmbuild.out
  exit 2
fi

# move the rpm to the current directory
mv -f $RPMNAME . && echo `basename $RPMNAME` created

# cleanup
rm -rf $WORKDIR rpmbuild.out
