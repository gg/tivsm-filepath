/* jbb.c -- Main ioctl handling functions
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


JBB.C 

Main utility portion of JBB.

jbb_ctl      - Called from hook.c driver_iotcl to perform parsing of XML commands passed in 
by the daemon.

jbb_log_data - Called by hooked functions to check if file changes need to be logged.  If so
jbb_doLog is called to write the data proper to the audit buffers/pools.

Chris Whalley
*/

#include "jbb.h"

static char *VfsAction2str[]={"Open","Unlink","Mkdir","Reduce",
    "Rmdir","Rename","Chattr","Read","Write",
    "Close","ChDate","Mknod","Link","Symlink"};

#define MAX_LIST (1024*4)
jbbStats g_jbbStats;
static char g_excludeList[MAX_LIST];

PF_Bool jbb_checkExcludeList(char *filename) {
    char *excl_list=jbb_getExcludeList();
    char *p;
    int n;

    while (excl_list) {
        p=strchr(excl_list,',');
        if (!p) p=strchr(excl_list,'|');
        if (p) n=p-excl_list;
        else n=strlen(excl_list);
        if (n && strncmp(filename,excl_list,n)==0) return 1;
        if (!p) return 0;
        excl_list=p+1;
    }
    return 0;  
}


PF_Bool jbb_checkWatchList(char *filename, WatchType_e type) {
    return watch_check(filename, type);
}



/*
Log data looks like this
<TSMLog file=\"$filename\" of=\"$otherfile\" vfsop=\"$vfsop\" mtime=\"$mtime\" dir=\"$dir\"/>
*/
#define LOG_STR_SIZE (PATH_MAX+64)
static int jbb_doLog(char *filename, char *toFilename, VfsAction_e vfsAction, PFTIME mtime, WatchType_e type) {
    int result = 0, size = 0, waitIfFull = 0;
    int found = 0, pool = 0;
    char temp[128];
    char *str = NULL;
    char paramList[WATCH_PARAM_LEN];
    static char logfile[ ] = { "??" };

    paramList[0] = 0;
    result = watch_getInfo(filename, type, &found, &pool, &waitIfFull, paramList);
    if (found && pool != 0 && paramList[0] != 0) {
        size = strlen(filename) + strlen(paramList) + 64;
        if (toFilename != NULL)
            size += strlen(toFilename);
        str = MEM_malloc(-8, size, "dlog");
        if (str) {
            snprintf(str, size, "%s", paramList);    
            STR_substitute(str, size, "$filename", filename);
            if (vfsAction == VfsRename_e && toFilename != NULL)
                STR_substitute(str, size, "$otherfile", toFilename); 
            else
                STR_substitute(str, size, "$otherfile", "");
            STR_substitute(str, size, "$vfsop", VfsAction2str[vfsAction]);
            snprintf(temp, sizeof(temp), "%ld", mtime);
            STR_substitute(str, size, "$mtime", temp);
            snprintf(temp, sizeof(temp), "%d", type);
            STR_substitute(str, size, "$dir", temp);

            result = audit_logToPool(logfile, str, pool, waitIfFull); /* Add to audit queue */
            MEM_free(str);
        }
        else result = FP_ERR_NOMEM;
    }
    return result;
}

int jbb_log_data(char *filename, char *toFilename, VfsAction_e vfsAction, PFTIME mtime, WatchType_e type) {
    int res = -1;
    if (jbb_checkExcludeList(filename) == 0 &&  jbb_checkWatchList(filename,type) == 1) {
        TRACE_Log(TRACE_AUDIT,"jbb_log_data: name:[%s] vfs:%s type:%s\n",
            filename, VfsAction2str[vfsAction], WatchType2str[type]);
        res = jbb_doLog(filename, toFilename, vfsAction, mtime, type);
        return res;
    }
    return res;
}


static FPRESULT parse_trace(char *str, int max) {
    // <trace mask=x,y,z device=SCREEN/FILE mode=XML>
    return TRACE_Parse_Common(str,max);
}


static FPRESULT parse_audit_get(char *buffer, int max) {
    int pool;
    PF32LONG moved=0;
    FPRESULT result;

    pool = xml_getAssignment32(buffer,"pool","0");
    result = audit_drainPool(buffer, max, pool, &moved);  
    if (moved && !result) {
        TRACE_Log(TRACE_AUDIT,"parse_audit_get: buffer %p pool %d moved %d\n",buffer,pool,moved);
        result=FP_INFO_XMLRESPONSE;
    }
    else if (result != 0) {
        LOG_Log(LOG_ERROR,"parse_audit_get: buffer %p pool %d result %d\n",buffer,pool,result);
    }
    return result;
}


static FPRESULT parse_audit_size(char *buffer, int max) {
    int pool;
    int size;
    FPRESULT result=FP_ERR_BADPARAM;

    pool=xml_getAssignment32(buffer,"pool","-1");
    if (pool >= 0) {
        size=xml_getAssignment32(buffer,"size","0");
        if (size >= 0) {
            result = audit_sizePool(size, pool, 1);
            if (result == FP_INFO_SUCCESS) {
                result = 0;
            }
        }
    }
    return result;
}


static FPRESULT parse_buffer_size(char *buffer, int max) {
    int pool;
    int size;
    FPRESULT result=FP_ERR_BADPARAM;

    pool=xml_getAssignment32(buffer,"pool","-1");
    if (pool >= 0) {
        size=xml_getAssignment32(buffer,"size","0");
        if (size >= 0) {
            result = audit_sizePool(size, pool, 0);
            if (result == FP_INFO_SUCCESS) {
                result = 0;
            }
        }
    }
    return result;
}


static FPRESULT parse_buffer_max(char *buffer, int max) {
    int size;
    FPRESULT result=FP_ERR_BADPARAM;

    size=xml_getAssignment32(buffer,"size","0");
    if (size >= 0) {
        result = audit_setMaxSize(size);
    }
    return result;
}


static FPRESULT parse_kversion(char *buffer, int max) {
    FPENTER(TRACE_XML,"parse_kversion","%s\n","");
    snprintf(buffer,max,"<kversion version=\"%s\"/>",JBB_VERSION);
    FPEXIT(TRACE_XML,"parse_kversion",FP_INFO_XMLRESPONSE);
}


static FPRESULT parse_walk(char *buffer, int max) {
    MEM_Walk("A"); 
    return FP_INFO_SUCCESS;
}

static FPRESULT parse_watch_dump(char *buffer, int max) {
    watch_dump(); 
    return FP_INFO_SUCCESS;
}

static FPRESULT parse_ir_dump(char *buffer, int max) {
    IR_dump(); 
    return FP_INFO_SUCCESS;
}

static FPRESULT parse_stats(char *buffer, int max) {
    FPRESULT i=0;
    jbbStats stats = g_jbbStats;

    *buffer=0;
    if (i==0) {
        snprintf(&buffer[strlen(buffer)],max-strlen(buffer),"<stats \n");
#undef STAT
#define STAT(_first, _second, _str, _var) \
    snprintf(&buffer[strlen(buffer)],max-strlen(buffer),"  %s=\"" PF_64d "\"\n", "$" _str, (PF64LONG)_var);
        ALL_STATS(s, max);
        snprintf(&buffer[strlen(buffer)],max-strlen(buffer),"/>\n");
        i = FP_INFO_XMLRESPONSE;
    }
    return i;
}


static int parse_clear(char *buffer, int max) {
    memset((char *)&g_jbbStats, 0, sizeof(g_jbbStats));
    return FP_INFO_SUCCESS;
}


/****** Exclude List Functions *******/
static void jbb_setExcludeList(char *list) {
    strncpy(g_excludeList, list, sizeof(g_excludeList));
    if (strlen(g_excludeList)>=sizeof(g_excludeList))
        LOG_Log(LOG_ERROR,"jbb_setExcludeList: exclude list too big\n","");
    g_excludeList[sizeof(g_excludeList)-1]=0; // force z
}

char *jbb_getExcludeList(void) {
    static int setup;
    if (!setup) {
        setup=1;
        jbb_setExcludeList(JBB_DEFAULT_EXCLUDE_LIST);
    }
    return g_excludeList;
}

static FPRESULT parse_exclude(char *buffer, int max) {
    /* <exclude list=one,two,three> */
    FPRESULT i=0;
    char temp[1024],*p;
    FPENTER(TRACE_XML,"parse_exclude","buffer:%s\n",buffer);
    xml_getAssignment(buffer,"list",temp,sizeof(temp),"");
    if (temp[0]) jbb_setExcludeList(temp);
    p=jbb_getExcludeList();
    snprintf(buffer,max,"<exclude list=\"%s\"/>",p);
    i=FP_INFO_XMLRESPONSE;
    FPEXIT(TRACE_XML,"parse_exclude",i);
}


/******* Watch List Fuctions ********/
static FPRESULT parse_rule(char *buffer, int max) {
    FPRESULT result = FP_INFO_SUCCESS;
    WatchType_e type = -1;
    char temp[32];
    char watch[WATCH_NAME_LEN];
    char *name = NULL;

    name = MEM_malloc(-8, PATH_MAX, "prul");
    if (!name)
        return FP_ERR_NOMEM;

    xml_getAssignment(buffer,"name",watch,sizeof(watch),"");
    xml_getAssignment(buffer,"meta.data",name,PATH_MAX,"");
    xml_getAssignment(buffer,"meta.select",temp,sizeof(temp),"");
    if (strcmp(temp,"files")==0) {
        type = WatchFile_e;
    }
    else if (strcmp(temp,"dirs")==0) {
        type = WatchDir_e;
    }
    else
    {
        MEM_free(name);
        return FP_ERR_BADPARAM;
    }
    result = watch_add(watch, name, type);

    MEM_free(name);
    return result;
}


static FPRESULT parse_rule_del(char *buffer, int max) {
    FPRESULT result = FP_INFO_SUCCESS;
    int pool = 0;
    char watch[WATCH_NAME_LEN];

    xml_getAssignment(buffer,"name",watch,sizeof(watch),"");
    if (*watch) {
        result = watch_remove(watch, &pool);
        audit_removePool(pool);
        TRACE_Log(TRACE_AUDIT,"parse_rule_del: Rule [%s] removed\n",watch);
    }
    else {
        LOG_Log(LOG_ERROR,"parse_rule: Undefined parameter watch:%p\n",watch);
        result = FP_ERR_BADPARAM;
    }
    return result;
}


static FPRESULT parse_action(char *buffer, int max) {
    FPRESULT result =  FP_ERR_BADPARAM;
    int pool = WATCH_POOL_UNDEFINED, waitIfFull = 0;
    char *p, paramList[WATCH_PARAM_LEN];

    pool = xml_getAssignment32(buffer,"log.pool","0");
    xml_getAssignment(buffer,"log.param",paramList,sizeof(paramList),"");
    result = audit_addPool(pool);
    if (result == 0) {
        p = strstr(buffer, "log.waitIfFull");
        if (p) waitIfFull = 1;
        result = watch_setInfo(pool,waitIfFull,paramList);
    }
    return result;
}


static FPRESULT parse_attachParty(char *buffer, int max) {
    int pool = xml_getAssignment32(buffer,"pool","0");
    audit_attachParty(pool);
    return FP_INFO_SUCCESS;
}


static FPRESULT jbb_parse_xml(char *buffer, int max) {
    FPRESULT result = FP_ERR_BADPARAM;
    if (xml_isKeyword("action",buffer))       return parse_action(buffer, max);
    if (xml_isKeyword("action-del",buffer))   return FP_INFO_SUCCESS;
    if (xml_isKeyword("audit-get",buffer))    return parse_audit_get(buffer, max);
    if (xml_isKeyword("audit-size",buffer))   return parse_audit_size(buffer, max); // Internal buffer will be 4x this size
    if (xml_isKeyword("buffer-size",buffer))  return parse_buffer_size(buffer, max); //Set the internal buffer size
    if (xml_isKeyword("buffer-max",buffer))   return parse_buffer_max(buffer, max); //Set the internal buffer max size
    if (xml_isKeyword("attach-party",buffer)) return parse_attachParty(buffer, max);
    if (xml_isKeyword("clear",buffer))        return parse_clear(buffer, max);
    if (xml_isKeyword("config-set",buffer))   return FP_INFO_SUCCESS;
    if (xml_isKeyword("exclude",buffer))      return parse_exclude(buffer, max);
    if (xml_isKeyword("ir-dump",buffer))      return parse_ir_dump(buffer, max);
    if (xml_isKeyword("kversion",buffer))     return parse_kversion(buffer, max);
    if (xml_isKeyword("rule",buffer))         return parse_rule(buffer, max);
    if (xml_isKeyword("rule-del",buffer))     return parse_rule_del(buffer, max);
    if (xml_isKeyword("rule-bind",buffer))    return FP_INFO_SUCCESS;
    if (xml_isKeyword("stats",buffer))        return parse_stats(buffer, max);
    if (xml_isKeyword("trace",buffer))        return parse_trace(buffer, max);
    if (xml_isKeyword("walk",buffer))         return parse_walk(buffer, max);
    if (xml_isKeyword("watch-dump",buffer))   return parse_watch_dump(buffer, max);
    return result;
}


FPRESULT jbb_ctl(PF_Ctl_t *c, char *data) {
    FPRESULT res=0;

    if (c == NULL || data == NULL) 
        return FP_ERR_BAD_IOCTL;
    FPENTER(TRACE_IOCTL,"jbb_ctl","c:" PF_P " cmd:%d nBytesIn:%d maxOut:%d\n",
        c, c->subCommand, c->nBytesIn, c->maxOut);
    c->nBytesOut=0;
    c->result = 0;
    switch (c->subCommand) {
    case fpaCtl_XML_e:
        res=jbb_parse_xml(data, c->maxOut);
        c->result = res;
        if (res==FP_INFO_XMLRESPONSE)
            c->nBytesOut = strlen(data)+1;
        break;
    default:
        FPERR_Announce(FP_ERR_BADIOCTL, c->subCommand);
        res=FP_ERR_BAD_IOCTL;
        c->result = res;
    } 
    FPEXIT(TRACE_IOCTL,"jbb_ioctl",res);
}

