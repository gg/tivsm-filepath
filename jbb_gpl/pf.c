/* pf.c -- Platform specific funtions.
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

PF.C - Includes the monitoring section to determine if a watching
process / task has exited without removing watches and pools
from memory.

Platform specific function wrappers for semaphore, mutex, memory, ... 

*/

#include <linux/version.h>
#include <linux/mm.h>
#include <linux/utime.h>
#include <linux/tty.h>
#include <linux/kernel.h>  /* printk() */
#include <linux/types.h>
#include <linux/slab.h>    /* kmalloc */
#include <linux/vmalloc.h> /* vmalloc */
#include <linux/sched.h>   /* current */
#include <linux/workqueue.h>
#include <linux/list.h>

#include "jbb.h"

#define MAX_KMALLOC (1*1024*1024)   // Only allow a max 128K of kmalloc memory at a time
#define USE_VMALLOC 0               // 1 for vmalloc only,  0 allow kmalloc if it fits
#define ALLOWED_KMALLOC_FAILURES 1  // kmalloc can fail this many times before we give up and only use vmalloc

void *PF_GetMem(unsigned int size, int *memType) {
#if USE_VMALLOC
    return (void *)vmalloc(size);
#else
    static int kmallocFailCount = 0;  // If kmalloc fails then use only vmalloc from now on.
    void *mem = NULL;
    if ((size < MAX_KMALLOC) && (kmallocFailCount < ALLOWED_KMALLOC_FAILURES)) {  // If kmalloc continues to fail give up on it
        mem = (void *)kmalloc(size,GFP_KERNEL);
        if (mem != NULL) {
            *memType = MemKmalloc_e;
            return mem;
        }
        else {  // Failure
            LOG_Log(LOG_ERROR,"PF_GetMem kmalloc failed size:%d. Failing over to vmalloc. kmallocFailCount:%d\n",size,kmallocFailCount);
            kmallocFailCount++;
            *memType = MemVmalloc_e;
            return (void *)vmalloc(size);  // Kmalloc failed so fall back on vmalloc
        }
    }
    else {
        *memType = MemVmalloc_e;
        return (void *)vmalloc(size);  // Needed just in case there are larger sizes
    }
#endif
    return (NULL);
}

void PF_FreeMem(void *p, int size, int memType) {
#if USE_VMALLOC
    vfree(p);
#else
    if (memType == MemKmalloc_e)
        kfree(p);
    else
        vfree(p);
#endif
}

int PF_SemInit(PF_SemType typ, void *mem, PF_Bool IsTaken) {
    struct semaphore *s=(struct semaphore *)mem;

    sema_init(s, IsTaken ? 0:1);
    return 0;
}


int PF_SemDestroy(void *mem) {
    return 0; /* ?? */
}

int PF_SemDown(void *mem, PF_Bool Interruptable) {
    struct semaphore *s=(struct semaphore *)mem;
    int i=0;
    if (Interruptable) {
        i=down_interruptible(s); 
        if (i) return FP_ERR_INTRPTD;
        else return 0;
    }

    down(s);
    return 0;
}


int PF_SemUp(void *mem) {
    struct semaphore *s=(struct semaphore *)mem;
    up(s);
    return 0;
}

int PF_MutexInit(PF_SemType typ, void *mem) {
    return PF_SemInit(typ, mem, 0);
}

int PF_MutexDestroy(void *mem) {
    return PF_SemDestroy(mem);
}

int PF_MutexHold(void *mem) {
    return PF_SemDown(mem,0);
}

int PF_MutexRelease(void *mem) {
    return PF_SemUp(mem);
}

void PF_Log(PF_LogTarget target, char *msg) {
    printk(msg);
}

PFTIME pf_secs(void) {
    struct timeval t;
    do_gettimeofday(&t);
    return t.tv_sec;
}

void PF_DelaySeconds(int secs) {
    PFTIME s=pf_secs();
    while (pf_secs()-s <= secs) {
        schedule();
    }
}

/***************************************************************************

*****  Process/Task Monitor *****

This is Very Liux specific.  It is called from audit.c by attachParty.
Its purpose is to monitor the health of the process that started an audit
buffer rule/watch.  If the caller dies then the buffer and rule must be
deleted.  Work Queues are started for each of the rules the caller creates.
The queues monitor the state of the task and calls audit_removePool if the 
caller dies.

*****************************************************************************/

#ifndef task_is_dead  // This macro does not exist in earlier versions.
#define task_is_dead(task) ((task)->exit_state != 0)
#endif

#define WORK_DELAY 3 // 3 second work_queue delay

#if ( LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19) ) // Not defined in 2.6.18 and below
# define TASK_DEAD 64  // Defined as 128 in 2.6.19 and changed to 64 in 2.6.24
#endif

#if ( LINUX_VERSION_CODE > KERNEL_VERSION(2,6,19) ) // 2.6.20 and above
typedef struct delayed_work* WORK_STRUCT;
typedef struct work_struct*  WORK_FUNC_ARG;
#else
typedef struct work_struct*  WORK_STRUCT;
typedef void*                WORK_FUNC_ARG;
#endif

typedef struct _attachData {
#if ( LINUX_VERSION_CODE > KERNEL_VERSION(2,6,19) ) // 2.6.20 and above
    struct delayed_work work;  // Yes must be here
#else
    struct work_struct  work;  // Yes must be here
#endif
    int                 state;
    int                 pool;
    void               *parentTask; // Task Structure
} attachData;

typedef struct _Monitor_Item {
    struct list_head   list;
    int                pool;
    attachData        *data;
} Monitor_Item;

static LIST_HEAD(Monitor_Head);
static char *Monitor_Mutex = NULL;
static void PF_MonitorFunc(WORK_FUNC_ARG data);

static void init_work(WORK_STRUCT data)
{
#if ( LINUX_VERSION_CODE > KERNEL_VERSION(2,6,19) ) // 2.6.20 and above
    INIT_DELAYED_WORK((WORK_STRUCT)data, PF_MonitorFunc);
#else
    INIT_WORK((WORK_STRUCT)data, PF_MonitorFunc, data);
#endif
}


// Called from module_init in hook.c
FPRESULT monitor_init(void) 
{
    FPRESULT result = 0;

    if (Monitor_Mutex) return 0; // Already setup
    Monitor_Mutex=MEM_malloc(-8,PF_SEM_SIZE,"irmu");
    if (!Monitor_Mutex) return FP_ERR_NOMEM;
    result=PF_MutexInit(PF_SemSystem, Monitor_Mutex);
    if (result) LOG_Log(LOG_ERROR,"Monitor_init: failed mutex init:%d\n",result);
    return result;
}


static void PF_MonitorFunc(WORK_FUNC_ARG data)
{
    attachData *ad = (attachData *)data;
    struct task_struct *parent = NULL;

    if (ad) {
        parent = ad->parentTask;  // Pointer to task_struct of process being monitored.
        if (parent) {
            TRACE_Log(TRACE_MONITOR,"PF_MonitorFunc: parentTask->pid:%d pool:%d isDead:%d\n",parent->pid,ad->pool,task_is_dead(parent));
            if (task_is_dead(parent) == 1) {
                LOG_Log(LOG_ERROR,"PF_MonitorFunc: Application %s exited! Cleaning up pool %d...\n",
                    parent->comm, ad->pool);
                watch_removeByPool(ad->pool);
                audit_removePool(ad->pool);
                ad->state = 0;
                return;
            }
        } 
        else {
            TRACE_Log(TRACE_MONITOR,"PF_MonitorFunc: parent %s \n","NULL");
            ad->state = 0;
            return;
        }
        schedule_delayed_work((WORK_STRUCT)data, (HZ*WORK_DELAY));  // delay WORK_DELAY seconds and loop/start again  
    }
    return;
}


void PF_StartMonitor(int pool) {
    Monitor_Item *monitor_item = NULL;
    attachData *data = NULL;
    int result = 0;

    if (watch_verifyByPool(pool)) {
        monitor_item = MEM_malloc(-8,sizeof(*monitor_item),"prid");
        if (monitor_item) {
            data = MEM_malloc(-8, sizeof(attachData),"pfad");
            if (data) {
                data->state = 1;
                data->parentTask = current;
                data->pool = pool;
                monitor_item->pool = pool;
                monitor_item->data = data;
                TRACE_Log(TRACE_MONITOR,"PF_StartMonitor: pool:%d task:%p \n", pool, data->parentTask);
                init_work((WORK_STRUCT)data);
                result = schedule_delayed_work((WORK_STRUCT)data, (HZ*WORK_DELAY)); //delay WORK_DELAY second
                TRACE_Log(TRACE_MONITOR,"PF_StartMonitor: monitor starte for pool:%d res:%d\n", pool,result);
                PF_MutexHold(Monitor_Mutex);
                list_add(&monitor_item->list, &Monitor_Head);
                PF_MutexRelease(Monitor_Mutex);        
            }
            else MEM_free(monitor_item);
        }
    }
}


void PF_StopMonitor(int pool) {
    Monitor_Item *monitor_item = NULL;

    PF_MutexHold(Monitor_Mutex);
    if(!list_empty(&Monitor_Head)){
        list_for_each_entry(monitor_item, &Monitor_Head, list) {
            if (monitor_item->pool == pool && monitor_item->data != NULL) {
                TRACE_Log(TRACE_MONITOR,"PF_StopMonitor: pool:%d task:%p \n",pool,monitor_item->data->parentTask);
                cancel_delayed_work((WORK_STRUCT)monitor_item->data);
                MEM_free(monitor_item->data);
                list_del(&monitor_item->list);
                MEM_free(monitor_item);
                monitor_item = NULL;
                break;
            }
        }
    }
    PF_MutexRelease(Monitor_Mutex);
}


void monitor_fini(void) 
{
    Monitor_Item *monitor_item = NULL, *next = NULL;
    if (Monitor_Mutex) {
        PF_MutexHold(Monitor_Mutex);
        if(!list_empty(&Monitor_Head)){
            list_for_each_entry_safe(monitor_item, next, &Monitor_Head, list) {
                if (monitor_item->data != NULL) {
                    cancel_delayed_work((WORK_STRUCT)monitor_item->data);
                    MEM_free(monitor_item->data);
                    list_del(&monitor_item->list);
                    MEM_free(monitor_item);
                }
                monitor_item = NULL;
                monitor_item = next;
            }
        }
        PF_MutexRelease(Monitor_Mutex);
        PF_MutexDestroy(Monitor_Mutex);
        MEM_free(Monitor_Mutex);
        Monitor_Mutex=0;
    }
}


