/* str_supp.c -- String manipulation functions.
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <linux/version.h>
#include "jbb.h"

static char *STR_insertStr(char *p, unsigned int max, char *str) {
    if (strlen(p)+strlen(str) >= max)  // if would overflow...
        return &p[strlen(p)]; // return ptr to end-of-string
    memmove(p+strlen(str),p,strlen(p)+1);
    memcpy(p,str,strlen(str));
    return p+strlen(str);
}


static void STR_removeChars(char *p, unsigned int n) {
    if (n>strlen(p)) 
        n=strlen(p);
    /*=== Replace memcpy with memmove defect 87022 ===*/
    memmove(p,p+n, strlen(p+n)+1);  
}

static char *STR_substitute1(char *linein, unsigned int max, char *match, char *insert) {
    char *p;
    char *last=0,*line;

    line=linein;
    p=strstr(line,match);
    if (!p) return 0;
    STR_removeChars(p,strlen(match));
    last=STR_insertStr(p,max-(p-linein),insert);
    line=last; /* prevent re-sub'ing on what we just dropped-in */
    return last;
}


int STR_substitute(char *linein, unsigned int max, char *match, char *insert) {
    char *p;
    int count=0;

    p=linein;
    while (1) {
        p=STR_substitute1(p, max-(p-linein), match, insert);
        if (!p) break;
        count++;
    }
    return count;
}


PF32LONG STR_tol(char *str) {
    PF32LONG x=0,mult=1;
    int isneg=0;
    /* We could add checks for 0x or other
    bases...later. */
    while (*str && *str<=' ') str++;
    if (*str == '-') {isneg=1; str++;}
    while (*str && *str>='0' && *str<='9') {
        x = x*10 + *str-'0';
        str++;
    }
    if (isneg) x= -x;
    if (*str == 'k' || *str=='K') mult=(PF32LONG)1024;
    if (*str == 'm' || *str=='M') mult=(PF32LONG)1024*1024;
    if (*str == 'g' || *str=='G') mult=(PF32LONG)1024*1024*1024;
    x = x*mult;
    return x;
}

int STR_atoi(char *str) {
    int x=0, isneg=0;

    while (*str && *str<=' ') str++;
    if (*str == '-') {isneg=1; str++;}
    while (*str && *str>='0' && *str<='9') {
        x = x*10 + *str-'0';
        str++;
    }
    if (isneg) x= -x;
    return x;
}


#if ( LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,18) ) // Not defined in 2.6.18 and below
static char upcase(char c) {
    if (c>='a' && c<='z') c=(c-'a')+'A';
    return c;
}
#endif

int STR_strcasecmp(const char *s1, const char *s2) {
#if ( LINUX_VERSION_CODE > KERNEL_VERSION(2,6,18) ) // Not defined in 2.6.18 and below
    return strcasecmp(s1, s2);
#else
    int c1, c2;
    do {
        c1 = upcase(*s1++);
        c2 = upcase(*s2++);
    } while (c1 == c2 && c1 != 0);
    return c1 - c2;
#endif
}


int STR_strncasecmp(const char *s1, const char *s2, int n) {
#if ( LINUX_VERSION_CODE > KERNEL_VERSION(2,6,18) ) // Not defined in 2.6.18 and below
    return strncasecmp(s1, s2, n);
#else
    int c1, c2;
    do {
        c1 = upcase(*s1++);
        c2 = upcase(*s2++);
    } while ((--n > 0) && c1 == c2 && c1 != 0);
    return c1 - c2;
#endif
}


