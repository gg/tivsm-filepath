
/* pf.h -- Platform specific functions
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
* Generalized abstractions.  Functions that have very specific 
* implementations on various platforms but are logically needed
* by the common code.
* 
* "PF" stands for Platform that is,
* these functions (and items in this directory) are for the 
* purpose of normalizing platform differences.
*/

#ifndef __PF_H
#define __PF_H

#ifdef __cplusplus
extern "C" {
#endif

    typedef long long PF64LONG;
    typedef long PF32LONG;
    typedef long PFPOINTER;

#define PF_P   "%p"   /* 'pointer' printf specifier */
#define PF_64x "%llx" /* Cap 'L' works in both user&kern old:"%llx" */
#define PF_64d "%lld" /* "%lld" */
#define PF_64_ "lld"  /* No percent...so user can specify field-width */

    typedef PF32LONG PF_Bool; /* Force a specifc bit-size due to comm needs */
    typedef PF32LONG FPRESULT;  /* All functions should return this type as a value */


    /***************************
    Interface for logging. Should only be
    used by fplog.c...all other codes should
    use fplog or fptrace or fperr functions.
    ***************************/
# define FPLOG_BUF_SIZE 1024
    typedef enum {PF_SysLog, PF_Terminal} PF_LogTarget;
    void PF_Log(PF_LogTarget target, char *msg);

    void *PF_GetMem(unsigned int size, int *memType);
    void PF_FreeMem(void *p, int size, int memType);  

    /*
    * Counting semaphores. Implementor must allow cooperation between
    * threads/processes (that is, one guy does the 'down' to wait for
    * something, the other guy provides the data/answer and does the 'up'.
    * Ups can (and will) happen before downs, sometimes, and thus the
    * down must not block. Hence, "counting semaphore". This is critical
    * behaviour.  This is not a mutex.
    */
# define PF_SEM_SIZE 128 
    typedef enum {PF_SemProcess, PF_SemSystem} PF_SemType;
    int PF_SemInit(PF_SemType typ, void *mem, PF_Bool IsTaken);
    int PF_SemDestroy(void *mem);
    int PF_SemDown(void *mem, PF_Bool Interruptable);
    int PF_SemUp(void *mem);

    /*
    * Classic mutex. To guard short areas of code. Implementor *may* 
    * decide to use the counting sems above to implement these and that's
    * just fine. Some platforms allow for tighter/faster mutex's though.
    */
    int PF_MutexInit(PF_SemType typ, void *mem);
    int PF_MutexDestroy(void *mem);
    int PF_MutexHold(void *mem);
    int PF_MutexRelease(void *mem);

    /***************************
    Misc
    ***************************/
    typedef PF32LONG PFTIME;       // Time value, in seconds. See pf_secs() below 
    PFTIME pf_secs(void);          // Current time, in seconds
    void PF_DelaySeconds(int secs);  // Sleep

    /***************************
    Linux Work Queue, because Threads spin out of control and cannot be delayed
    ***************************/

    void PF_StartMonitor(int pool);
    void PF_StopMonitor(int pool); 
    void monitor_fini(void);
    FPRESULT monitor_init(void);


#ifdef __cplusplus
}
#endif

#endif
