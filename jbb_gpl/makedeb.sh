#!/bin/sh

RELNUM=$1

DEBDIR=tivsm-filepath-$RELNUM

rm -rf $DEBDIR

mkdir -p $DEBDIR/DEBIAN
chmod 755 $DEBDIR/DEBIAN
cat deb/control | sed "s/%arch%/`dpkg --print-architecture`/" | sed "s/%version%/$RELNUM/" > $DEBDIR/DEBIAN/control
cp -a deb/preinst  $DEBDIR/DEBIAN/
cp -a deb/postinst $DEBDIR/DEBIAN/
cp -a deb/prerm    $DEBDIR/DEBIAN/

mkdir -p $DEBDIR/opt/filepath

cp filepath filepath.ko $DEBDIR/opt/filepath/

dpkg-deb --build $DEBDIR

rm -rf $DEBDIR
