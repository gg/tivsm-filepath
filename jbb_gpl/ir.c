/**
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


IR.C

Every time a file is open it is checked to see if it matches a watch
criteria.  If it is a match then an IR_Item is added for the file.
The IR_Item tracks mtime and dirty state of the file and is updated 
by calls to IR_SetAttr from the hook functions.  Subsquently IR_GetAttr
is called when a file is close to see if the file meets the rules 
set for adding its changed data to the audit buffer.

*/

#include <linux/types.h>
#include <linux/string.h>
#include <linux/list.h>
#include "jbb.h"

typedef struct _IR_Item {
    struct list_head  list;
    IR_Identifier     identifier;
    int               dirtyFlag;
    PFTIME            mtime;
    char              name[1];  // variable length array
} IR_Item;

static LIST_HEAD(IR_Head);
static char *IR_Mutex = NULL;

// Called from module_init in hook.c
FPRESULT IR_init(void) 
{
    FPRESULT result = 0;

    if (IR_Mutex) return 0; // Already setup
    IR_Mutex=MEM_malloc(-8,PF_SEM_SIZE,"irmu");
    if (!IR_Mutex) return FP_ERR_NOMEM;
    result=PF_MutexInit(PF_SemSystem, IR_Mutex);
    if (result) {
        LOG_Log(LOG_ERROR,"IR_init: failed mutex init:%d\n",result);
        return result;
    }
    return result;
}

void IR_fini(void) 
{
    IR_Item *ir_item = NULL, *next = NULL;
    if (IR_Mutex) {
        PF_MutexHold(IR_Mutex);
        if(!list_empty(&IR_Head)){
            list_for_each_entry_safe(ir_item, next, &IR_Head, list) {
                list_del(&ir_item->list);
                MEM_free(ir_item);
            }
        }
        PF_MutexRelease(IR_Mutex);
        PF_MutexDestroy(IR_Mutex);
        MEM_free(IR_Mutex);
        IR_Mutex = NULL;
    }
}

FPRESULT IR_Add(char *name, IR_Identifier identifier, PFTIME mtime, int dirty) 
{
    IR_Item *ir_item = NULL;
    FPRESULT result=0;
    int found = _FALSE, namelen = 0;

    if (name == NULL) return result;
    namelen = strlen(name);
    TRACE_Log(TRACE_IR,"IR_Add: %p [%s] mtime:%d dirty:%d\n",identifier,name,mtime,dirty);
    PF_MutexHold(IR_Mutex);
    if (dirty == NOT_DIRTY) { // Open is calling. See if create has already called.
        if(!list_empty(&IR_Head)){
            list_for_each_entry(ir_item, &IR_Head, list) {
                if (ir_item->identifier == identifier){
                    TRACE_Log(TRACE_IR,"IR_Add: [%s] was added by create already.\n",name);
                    found = _TRUE;
                    break;  // Already exists
                }
            }
        }
    }
    if (found == _FALSE) {
        ir_item = MEM_malloc(-8,sizeof(*ir_item)+namelen,"irad");
        if (!ir_item) result = FP_ERR_NOMEM;
        else {
            g_jbbStats.infoRecDepth++;
            strcpy(ir_item->name,name);
            ir_item->identifier = identifier;
            ir_item->mtime = mtime;
            ir_item->dirtyFlag = dirty;
            list_add(&ir_item->list, &IR_Head);
        }
    }
    PF_MutexRelease(IR_Mutex);
    return result;
}

FPRESULT IR_SetAttr(char *name, IR_Identifier identifier, PFTIME mtime, int dirty) 
{
    FPRESULT found = _FALSE;
    IR_Item *ir_item = NULL;

    if (jbb_checkExcludeList(name) == 0) {      
        PF_MutexHold(IR_Mutex);
        if(!list_empty(&IR_Head)){
            list_for_each_entry(ir_item, &IR_Head, list) {
                if (ir_item->identifier == identifier){

                    TRACE_Log(TRACE_IR,"IR_SetDirty: %p [%s] %d\n",identifier,ir_item->name,ir_item->dirtyFlag);
                    
                    switch (ir_item->dirtyFlag)
                    {
                    case NOT_DIRTY:
                        ir_item->dirtyFlag = dirty;
                    break;
                    case DIRTY_ATTR:
                       if (dirty == DIRTY_DATA)
                        ir_item->dirtyFlag = dirty;
                    break;
                    case DIRTY_DATA:
                    default:
                    break;
                    }
                    if (mtime != 0)
                        ir_item->mtime = mtime;
                    found = _TRUE;
                    break; // Found what was need so exit for loop

                }
            }
        }
        PF_MutexRelease(IR_Mutex);
    }
    return found;
}


FPRESULT IR_GetDirty(char *name, IR_Identifier identifier, int *dirty, PFTIME *mtime) 
{
    FPRESULT result = _FALSE;
    IR_Item *ir_item = NULL;

    if (name == NULL || dirty == NULL || mtime == NULL) return result;
    if (jbb_checkExcludeList(name) == 0) {      
        PF_MutexHold(IR_Mutex);
        if(!list_empty(&IR_Head)){
            list_for_each_entry(ir_item, &IR_Head, list) {
                if (ir_item->identifier == identifier){
                    g_jbbStats.infoRecDepth--;
                    TRACE_Log(TRACE_IR,"IR_GetDirty: %p [%s] %d %d\n",identifier,ir_item->name,ir_item->dirtyFlag,ir_item->mtime);
                    *dirty = ir_item->dirtyFlag;
                    *mtime = ir_item->mtime;
                    list_del(&ir_item->list); // record did its job so remove it
                    MEM_free(ir_item);
                    result = _TRUE; // Yes the file was found
                    break; // Found what was need so exit for loop
                }
            }
        }
        PF_MutexRelease(IR_Mutex);
    }
    return result;
}


void IR_dump(void) 
{
    IR_Item *ir_item = NULL;

    PF_MutexHold(IR_Mutex);
    if(!list_empty(&IR_Head)){
        list_for_each_entry(ir_item, &IR_Head, list) {
            LOG_Log(LOG_PRINT,"IR_dump: name:[%s] dirty:%s mtime:%d\n",
                ir_item->name,
                ir_item->dirtyFlag?"Yes":"No",
                ir_item->mtime);
        }
    }
    PF_MutexRelease(IR_Mutex);
}

