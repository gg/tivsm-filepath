/* trace.h -- Tracing definitions
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef __TRACE_H
#define __TRACE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "jbb.h"

    typedef PF64LONG TRACE_Mask;  /* Be sure this has enough bits for all the codes...*/

    // WARNING: ANY change to the list below must update trace.c translation areas as well!!
#define TRACE_DEBUG1    ((TRACE_Mask)1 << 0)
#define TRACE_HOOK      ((TRACE_Mask)1 << 1)
#define TRACE_LOG       ((TRACE_Mask)1 << 2)
#define TRACE_AUDIT     ((TRACE_Mask)1 << 3)
#define TRACE_XML       ((TRACE_Mask)1 << 4)
#define TRACE_ERR       ((TRACE_Mask)1 << 5)
#define TRACE_HASH      ((TRACE_Mask)1 << 6)
#define TRACE_IOCTL     ((TRACE_Mask)1 << 7)
#define TRACE_PF        ((TRACE_Mask)1 << 8)
#define TRACE_WATCH     ((TRACE_Mask)1 << 9)
#define TRACE_OPEN      ((TRACE_Mask)1 << 10)
#define TRACE_CLOSE     ((TRACE_Mask)1 << 11)
#define TRACE_WRITE     ((TRACE_Mask)1 << 12)
#define TRACE_READ      ((TRACE_Mask)1 << 13)
#define TRACE_DIR       ((TRACE_Mask)1 << 14)
#define TRACE_SETATTR   ((TRACE_Mask)1 << 15)
#define TRACE_FSYNC     ((TRACE_Mask)1 << 16)
#define TRACE_RENAME    ((TRACE_Mask)1 << 17)
#define TRACE_UNLINK    ((TRACE_Mask)1 << 18)
#define TRACE_IR        ((TRACE_Mask)1 << 19)
#define TRACE_MONITOR   ((TRACE_Mask)1 << 20)
#define TRACE_MKNOD	    ((TRACE_Mask)1 << 21)
#define TRACE_LINK      ((TRACE_Mask)1 << 22)
#define TRACE_SYMLINK   ((TRACE_Mask)1 << 23)
#define TRACE_AUDITOUT  ((TRACE_Mask)1 << 24)
#define TRACE_CREATE    ((TRACE_Mask)1 << 25)


    // WARNING: ANY change to the list above must update trace.c translation areas as well!!

    typedef enum {TRACE_LevelCoarse_e, TRACE_LevelMedium_e, TRACE_LevelFine_e} TRACE_Level_t;

    int LogOn(TRACE_Mask code);

    void TRACE_Set(TRACE_Mask mask); 
    FPRESULT TRACE_SetByStr(char *code);
    void TRACE_AddBit(TRACE_Mask code);  /* Adds-in just one facility */
    void TRACE_Get(TRACE_Mask *mask);
    void TRACE_mask2str(TRACE_Mask mask, char *str, int max);
    void TRACE_LogDo(TRACE_Mask code, int line, char *file, char *format, ...);

    FPRESULT TRACE_SetLevelByStr(char *level);
    char *TRACE_GetLevelStr(void);

    FPRESULT TRACE_Parse_Common(char *str, int max);

    void fpenter(TRACE_Mask trace, int line, char *file, char *func, char *format, ...);
    void fpexit(TRACE_Mask trace, int line, char *file, char *func, FPRESULT result);
    void fpexitp(TRACE_Mask trace, int line, char *file, char *func, void *result);

#define FPEXIT(trace,func,res) { fpexit(trace,__LINE__, __FILE__,func,res); return res;}
#define FPEXITP(trace,func,res) { fpexitp(trace,__LINE__, __FILE__,func,res); return res;}

#define FPENTER(trace, func, format, ...) { \
    fpenter(trace, __LINE__, __FILE__, func, format, __VA_ARGS__); \
    }
#define TRACE_Log(code, format, ...) { \
    TRACE_LogDo(code, __LINE__, __FILE__, format, __VA_ARGS__); \
    }


#ifdef __cplusplus
}
#endif

#endif
