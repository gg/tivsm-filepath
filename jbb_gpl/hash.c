/* hash.c -- Hashed list functions
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/

#include <linux/list.h>
#include "jbb.h"

typedef struct HASH_Item_Struct { 
    struct list_head list;
    void *key;
    void *data;
    struct HASH_Item_Struct *next;
    char keydata[1]; /* Dynamic; Hold string keys */
} HASH_Item;

struct HASH_Struct {
    HASH_Item **items;  /* pointer to array, of size 'nitems' */
    char mutex[PF_SEM_SIZE];
    char name[32];
    HASH_Type_e typ;
    int count;    /* Current number of actual items being held */
    int nitems;   /* Number of rows allocated */
    int nbits;    /* Essentially, 'nitems' LOG(2) */
    int nCharsForKey; /* For string type */
    int dummy;    /* talk to whal...solaris/64 align for next datum */
    struct list_head list;
    char data[1]; /* dynamic area that 'items' points into...*/
};

#define DEFAULT_NROWS 1024*2

static int calc_nbits(int n) {
    int nbits=0;
    while (n) {
        nbits++;
        n = n>>1;
    }
    return nbits;
}


/*
* Create a new hash structure.
*/
FPRESULT HASH_Create(HASH_Type_e typ, int hint1, int hint2, HASH_ID *idptr, char *name) {
    int n; 

    FPENTER(TRACE_HASH,"HASH_Create","type:%d hint1:%d hint2:%d name:%s\n",
        typ, hint1, hint2, name);
    if (!idptr) 
        FPEXIT(TRACE_HASH,"HASH_Create",FP_ERR_HASHNULL);
    if (typ == HASH_Type_String_e && !hint1) 
        FPEXIT(TRACE_HASH,"HASH_Create",FP_ERR_HASHNULL);

    n = hint1;
    if (n==0) n=DEFAULT_NROWS;
    *idptr = MEM_malloc(-8, sizeof(**idptr) + n * sizeof(HASH_Item *), name);
    if (!*idptr) 
        FPEXIT(TRACE_HASH,"HASH_Create",FP_ERR_NOMEM);
    (*idptr)->nitems = n;
    (*idptr)->nbits = calc_nbits(n);
    (*idptr)->items = (HASH_Item **)&(*idptr)->data;
    (*idptr)->typ = typ;
    (*idptr)->nCharsForKey = hint1;
    INIT_LIST_HEAD(&(*idptr)->list);
    strncpy((*idptr)->name, name, sizeof((*idptr)->name));
    PF_MutexInit(PF_SemProcess, (*idptr)->mutex);
    TRACE_Log(TRACE_HASH,"Hash ID: " PF_P "\n",*idptr);
    FPEXIT(TRACE_HASH,"HASH_Create",0);
}


FPRESULT HASH_DisposeAndClear(HASH_ID *id) {
    int i;

    if (!id || !*id) 
        FPEXIT(TRACE_HASH,"HASH_Dispose",FP_ERR_HASHNULL);

    FPENTER(TRACE_HASH,"HASH_Dispose","id:" PF_P " %s\n",(*id),(*id)->name);
    for (i=0; i<(*id)->nitems; i++) {
        HASH_Item *cur, *next;
        for (cur = (*id)->items[i]; cur; cur = next) {
            next = cur->next;
            MEM_free(cur);
        }
    }
    PF_MutexDestroy((*id)->mutex);
    MEM_free((void *)*id);
    *id = 0;
    FPEXIT(TRACE_HASH,"HASH_Dispose",0);
}


static int HASH_AddItem(HASH_ID id, int where, void *key, void *data) {
    int i=0;

    if (!id) return FP_ERR_HASHNULL;
    PF_MutexHold(id->mutex);
    /* This section just does a "push"...faster */
    {
        HASH_Item *item;
        int extra=0;
        if (id->typ == HASH_Type_String_e) 
            extra = strlen(key)+1;
        item = MEM_malloc(-8, sizeof(HASH_Item)+extra, "Hitm");
        if (!item) i=FP_ERR_NOMEM;
        else {
            item->key = key;
            item->data = data;
            item->next = id->items[where];
            if (id->typ == HASH_Type_String_e) {
                item->key = item->keydata;
                strncpy(item->keydata, key, extra);
            }
            id->items[where] = item;
            list_add(&item->list, &id->list);
        }
    }
    id->count++;
    PF_MutexRelease(id->mutex);
    return i;
}


static int GetIdxForStringType(HASH_ID id, char *str_in) {
    /*
    * Sum-up the entire string, then mod it by the
    * number of elements in the array. This turns out to
    * be pretty good.  Say, a 10 char key-string with an average
    * value of 'a' (about 100) makes about 1,000.  Are default
    * array has 2000 elements. Pretty good distribution.
    */
    int sum=0;
    unsigned char *str=(unsigned char *)str_in;

    if (str) 
        for (;*str; str++) sum+= *str;

    sum = sum % id->nitems;
    if (sum < 0) sum = -sum;
    return sum;
}


static int HASH_GetIdx(HASH_ID id, void *key) {
    PFPOINTER p, mask;

    if (!id) return 0;
    if (id->typ == HASH_Type_String_e) 
        return GetIdxForStringType(id, (char *)key);

    if (id->typ == HASH_Type_Int_e) {
        return ((unsigned long)key) % id->nitems;
    }

    if (sizeof(p) != sizeof(void *))
        LOG_Log(LOG_PANIC,"HASH_Add: Pointer value not created correctly!\n","");

    /* In general, the low order 3
    bits of a pointer are always zero. So we'll shift those out.
    */
    p = (PFPOINTER)key;
    p = p >> 3;
    /* We'll use the next NBITS low-order bits as the index into our array. */
    mask = (1 << (id->nbits-1)) - 1;
    p = p & mask;

    if (p >= id->nitems) {
        LOG_Log(LOG_ERROR,"HASH_GetIdx: too far. nbits:%d nitems:%d p:" PF_P "(%d) key: " PF_P "\n",
            id->nbits, id->nitems, p, (int)p, key);
        p = id->nitems-1;
    }
    return (int)p;
}



FPRESULT HASH_AddPrim(HASH_ID id, void *key, void *data, int line, char *file) {
    int idx;
    FPRESULT result;

    if (!id) return (FP_ERR_HASHNULL);

    FPENTER(TRACE_HASH,"HASH_Add","ID:" PF_P " name:%s key:" PF_P " data:" PF_P "\n",
        id,id->name,key,data);
    TRACE_Log(TRACE_HASH,"HASH_ADD line:%d  file:%s\n", line, file);
    idx = HASH_GetIdx(id, key);
    TRACE_Log(TRACE_HASH,"GetIdx result: %d\n",idx);
    result=HASH_AddItem(id, idx, key, data);
    FPEXIT(TRACE_HASH,"HASH_Add",result);
}


void *HASH_Find(HASH_ID id, void *key, HASH_OPTION andDelete) {
    int idx;
    HASH_Item **item;
    void *result=0;

    if (!id) return 0;
    FPENTER(TRACE_HASH,"HASH_Find","id:" PF_P " name:%s key:" PF_P " andDelete:%d\n",
        id, id->name, key, andDelete);
    PF_MutexHold(id->mutex);
    idx = HASH_GetIdx(id, key);
    for (item = &id->items[idx]; *item; item = &(*item)->next)
        if ((id->typ != HASH_Type_String_e && (*item)->key == key) ||
            (id->typ == HASH_Type_String_e && strcmp(key,(*item)->key)==0))  {
                HASH_Item *tofree;
                tofree = *item;
                result = (*item)->data;
                if (andDelete == HASH_REMOVE) {
                    list_del((&(*item)->list));
                    *item = (*item)->next;
                    id->count--;
                    MEM_free(tofree);
                }
                if (andDelete == HASH_POP) {
                    // Pop this item in the ring list
                    list_del((&(*item)->list));
                    list_add(&(*item)->list, &id->list);
                }
                break;
        }
        PF_MutexRelease(id->mutex);
        FPEXITP(TRACE_HASH,"HASH_Find",result);
}


static void *HASH_FindKeyByIdx(HASH_ID id, int which) {
    void *key=0,*result=0;
    HASH_Item *item;
    int i=0,idx,found=0;

    if (!id) return 0;
    FPENTER(TRACE_HASH,"HASH_FindByIdx","id:" PF_P " name:%s which:%d\n",
        id, id->name, which);
    PF_MutexHold(id->mutex);
    for (idx = 0; idx < id->nitems; idx++) {
        item = id->items[idx];
        if (item) i++;
        else continue;
        while (i < which) {
            item = item->next;
            if (item) i++;
            if (!item) break;
        }
        if (which == i) {
            if (item) {
                key = item->key;
                found=1;
            }
            break;
        }
    }
    PF_MutexRelease(id->mutex);
    if (found) result=key;
    FPEXITP(TRACE_HASH,"HASH_FindByIdx",result);
}



void *HASH_FindByIdx(HASH_ID id, int which, HASH_OPTION andDelete) {
    void *key=0,*result=0;

    if (!id) return 0;
    FPENTER(TRACE_HASH,"HASH_FindByIdx","id:" PF_P " name:%s which:%d andDelete:%d\n",
        id, id->name, which, andDelete);
    key = HASH_FindKeyByIdx(id, which);
    if (key) result=HASH_Find(id, key, andDelete);
    FPEXITP(TRACE_HASH,"HASH_FindByIdx",result);
}








