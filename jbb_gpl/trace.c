/* trace.c -- trace handling.  Allows for trace definitions in trace.h
*            to be turned on and off and eventually sent to the logging 
*            system to be displayed.
*
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/

#include "jbb.h"

static TRACE_Mask     tracemask = 0;
static TRACE_Level_t  traceLevel = TRACE_LevelFine_e;  
static char           *TRACE_Level2str[]={"Coarse","Medium","Fine"};

static int STR_str2enum(char *str, char **xlatArray, int nelements) {
    /* Returns -1 if fail, else index into the string-translation
    array (which is really the enum value) */
    int i;

    for (i=0; i<nelements; i++)
        if (STR_strcasecmp(str,xlatArray[i])==0) return i;
    return -1;
}


#define NELEMENTS(ary) (sizeof(ary)/sizeof(ary[0]))
#define XLATE(str,ary) STR_str2enum(str, ary, NELEMENTS(ary))


FPRESULT TRACE_SetLevelByStr(char *levelstr) {
    TRACE_Level_t level;
    level = XLATE(levelstr, TRACE_Level2str);
    if (level < 0) return FP_ERR_BADPARAM;
    traceLevel = level;
    return 0;
}

char *TRACE_GetLevelStr(void) {
    return TRACE_Level2str[traceLevel];
}

typedef struct {
    TRACE_Mask	value;
    char		name[32];
} Codes_t;

static Codes_t codes[] = {
    {TRACE_DEBUG1, "DEBUG1"},
    {TRACE_HOOK, "HOOK"},
    {TRACE_LOG, "LOG"},
    {TRACE_AUDIT, "AUDIT"},
    {TRACE_XML, "XML"},
    {TRACE_ERR, "ERR"},
    {TRACE_HASH, "HASH"},
    {TRACE_IOCTL, "IOCTL"},
    {TRACE_PF, "PF"},
    {TRACE_WATCH, "WATCH"},
    {TRACE_OPEN, "OPEN"},
    {TRACE_CLOSE, "CLOSE"},
    {TRACE_WRITE, "WRITE"},
    {TRACE_READ, "READ"},
    {TRACE_DIR, "DIR"},
    {TRACE_SETATTR, "SETATTR"},
    {TRACE_FSYNC, "FSYNC"},
    {TRACE_RENAME, "RENAME"},
    {TRACE_UNLINK, "UNLINK"},
    {TRACE_IR, "IR"},
    {TRACE_MONITOR, "MONITOR"},
    {TRACE_MKNOD, "MKNOD"},
    {TRACE_LINK, "LINK"},
    {TRACE_SYMLINK, "SYMLINK"},
    {TRACE_AUDITOUT, "AUDITOUT"},
    {TRACE_CREATE, "CREATE"},
};


#define NELEMENTS(ary) (sizeof(ary)/sizeof(ary[0]))

static char *code2str(TRACE_Mask code) {
    int i;

    for (i=0; i<NELEMENTS(codes); i++)
        if (codes[i].value & code) return codes[i].name;
    return "??err??";
}

static PF_Bool modeXML;
static PF_Bool modeIndent=1;
static PF_Bool showCode=1;

int LogOn(TRACE_Mask code) {
    return (code & tracemask);
}

static void TRACE_Logv(int deltaIndent, TRACE_Mask code, 
    char *func, char *format, va_list ap, TRACE_Level_t level, int line, char *file) {
        static int indent;

        if (level > traceLevel) return;

        if (code & tracemask) {
            static char buff[1024*5];  /* NOT reentrant...small kernel stacks */
            int i,target;
            char *filename = NULL;
            char loc[1024];
            loc[0] = 0;
            if (file) filename =  strrchr(file,'/');
            if (filename) snprintf(loc, 1024, "[%-7s %3d]", filename+1, line);

            buff[0]=0;
            if (modeXML) {
                strcat(buff,"<trace ");
            }

            if (modeIndent && !modeXML) {
                if (STR_strncasecmp(func,"hook_",5)==0 && deltaIndent) indent=0;
                if (deltaIndent < 0) target = indent+deltaIndent;
                else target = indent;
                if (target < 20) /* Just in case things run-away...*/
                    for (i=0; i<target; i++) strcat(buff," ");
                indent += deltaIndent;
                if (indent<0) indent=0;
                if (indent>20) indent=20;
            }

            if (showCode) {
                if (modeXML) {
                    snprintf(&buff[strlen(buff)],sizeof(buff)-strlen(buff),
                        "code=\"%s\" ",code2str(code));
                }
                else {
                    strcat(buff, code2str(code));
                    strcat(buff," ");
                }
            }

            /* Put the "preface" on the output line */
            if (func && *func) {
                if (modeXML) {
                    snprintf(&buff[strlen(buff)],sizeof(buff)-strlen(buff),
                        "func=\"%s\" ",func);
                }
                else {
                    strcat(buff, func);
                    strcat(buff," ");
                }
            }

            if (modeXML) {
                char *c;
                strcat(buff,"msg=\"");
                /* Remove any carriage return that is in the msg */
                c=strchr(format,'\n');
                if (c) *c=' ';
                vsnprintf(&buff[strlen(buff)],sizeof(buff)-10,format,ap);  
                strcat(buff,"\"/>\n");
                if (c) *c='\n';
            }
            else {
                int len = strlen(buff);
                vsnprintf(&buff[len],sizeof(buff)-len-10,format,ap);  
            }
            buff[sizeof(buff)-1]=0;  /* Ensure z-byte */

            LOG_Log(fpErrorLevel_Print_e, "%s %s", loc, buff);
        }
}

void fpenter(TRACE_Mask code, int line, char *file, char *func, char *format, ...) {
    va_list ap; 
    va_start(ap, format);  
    TRACE_Logv(1, code, func, format, ap, TRACE_LevelMedium_e, line, file);
    va_end(ap);
}

static void TRACE_Logf(TRACE_Mask code, int line, char *file, char *func, char *format, ...) {
    va_list ap; 
    va_start(ap, format);  
    TRACE_Logv(-1, code, func, format, ap, TRACE_LevelMedium_e, line, file);
    va_end(ap);
}

void fpexit(TRACE_Mask code, int line, char *file, char *func, FPRESULT result){
    TRACE_Logf(code, line, file, func, "Result:%d %s\n", result,FPERR_err2mnemonic(result));
}

void fpexitp(TRACE_Mask code, int line, char *file, char *func, void *result){
    TRACE_Logf(code, line, file, func, "Result:" PF_P "\n", result);
}

void TRACE_LogDo(TRACE_Mask code, int line, char *file, char *format, ...) {

    va_list ap; 
    va_start(ap, format);  
    TRACE_Logv(0, code, "", format, ap, TRACE_LevelFine_e, line, file);
    va_end(ap);
}

void TRACE_Get(TRACE_Mask *mask) {
    if (mask) *mask = tracemask;
}


void TRACE_mask2str(TRACE_Mask mask, char *str, int max) {
    unsigned int i;
    int doneAtLeastOne=0;
    *str=0;
    for (i=0; i<NELEMENTS(codes); i++) {
        if (codes[i].value & mask) {
            if (doneAtLeastOne) { 
                strcat(str,","); 
                str++; 
                max--;
            }
            snprintf(str,max,"%s",codes[i].name);
            doneAtLeastOne=1;
            max-=strlen(str); str+=strlen(str); 
            if (max < 10) return;
        }
    }
}

void TRACE_AddBit(TRACE_Mask code) {
    tracemask |= code;
}

void TRACE_Set(TRACE_Mask mask) {
    tracemask = mask; 
}

FPRESULT TRACE_SetByStr(char *code) {
    /* "code" can be a comma separated list
    */
    TRACE_Mask c=0;
    unsigned int i;
    char *p=0;
    char temp[30];

    /* Break it up by the commas or vertbars...if any */
    while (1) {
        p=strchr(code,',');
        if (!p) p=strchr(code,'|');
        if (!p) strncpy(temp, code, sizeof(temp));
        else {
            strncpy(temp, code, sizeof(temp));
            temp[p-code]=0;
        }
        if (temp[0]) { /* If something specified... */
            if (STR_strcasecmp("all",temp)==0) {
                c = -1;  /* All bits 'on' */
                c = c - TRACE_DEBUG1;
            }
            else {
                for (i=0; i<NELEMENTS(codes); i++) {
                    if (STR_strcasecmp(codes[i].name,temp)==0) {
                        c |=  codes[i].value;
                        break;
                    }
                }
                if (i == NELEMENTS(codes)) return FP_ERR_IVLOGLVL;
            }
        } 
        if (!p) break;
        code=p+1;
    }
    TRACE_Set(c);
    return 0;
}

static FPRESULT TRACE_getXmlTraceCommand(char *str, int max){
    FPRESULT res = 0;
    PF_LogTarget target;
    TRACE_Mask mask;
    char maskstr[256];
    LOG_GetTarget(&target);
    TRACE_Get(&mask);
    TRACE_mask2str(mask,maskstr,sizeof(maskstr));
    snprintf(str,max,"<trace device=\"%s\" mask=\"%s\" level=\"%s\"/>",
        target==PF_SysLog?"file":"terminal", maskstr, TRACE_GetLevelStr());
    return res;
}

FPRESULT TRACE_Parse_Common(char *str, int max){
    FPRESULT i=0;
    char temp[128];

    xml_getAssignment(str,"mask",temp,sizeof(temp),"!");
    if (temp[0]=='!') {
        TRACE_getXmlTraceCommand(str,max);
        return FP_INFO_XMLRESPONSE;
    }
    if (!i && temp[0]!='!') 
        i=TRACE_SetByStr(temp);
    return i; 
}
