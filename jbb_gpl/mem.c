/* mem.c -- Memory tracking and handling
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

MEM.C

Memory tracking routines.  
Tracks memory alloctions by keeping a linked list of all memory that JBB allocates.
Watches for both memory under-runs and over-runs.  

Allocated memory can be examined by calling the "fpa walk" command.

*/

#include <linux/list.h>

#include "jbb.h"

#define JBBMEM_DBG TRACE_DEBUG1
#define SEM_UNLOCK 0
#define SEM_LOCK 1
#define SEM_DESTROY 2
#define MAX_ALIGN 8192

#define MEM_HEAD_TAG 0XABCDDCBA
#define MEM_TAIL_TAG 0XFEABBAEF
#define MEM_START_TAG 0XAABBCCDD


// Can not be static in Solaris
char g_mem_sem_data[128];
void *g_mem_sem=(void *)g_mem_sem_data;
#define ROUND_UP(base,num) ((long)num + (((long)base-1L) & ~((long)num-1L))) /* works both ways (512, value) and (value, 512)*/

static int g_sizeofMem;  // sizeof mem rounded up by 8 bytes
static LIST_HEAD(Mem_Head);

static char *MemType2str[]={"kmalloc","vmalloc"};


static void MEM_sem(int mode)
{
    static int count, init;
    if (init == 0) {
        PF_SemInit(0,(void *)g_mem_sem,0);
        init++;
    }

    if (mode == SEM_LOCK) {
        PF_SemDown(g_mem_sem,0);
        if (count++ > 0) LOG_Log(LOG_PANIC,"MEM_sem lock entered twice!!!","");
    }
    else if (mode == SEM_UNLOCK) {
        count--;
        PF_SemUp(g_mem_sem);
    }
    else {
        PF_SemDestroy(g_mem_sem);
        init--;
    }
}


static char *tag(char *t) {
    static char s[5];
    memcpy(s,t,4);
    s[4]=0;
    return s;
}


static void *getmem(unsigned int size, int *memType)
{
#ifdef _KERNEL
    return PF_GetMem(size, memType);
#else
    return (void *)malloc(size);  
#endif
}

static void freemem(mem *m)
{
#ifdef _KERNEL
    PF_FreeMem(m->orig,m->size,m->memType);
#else
    free(m->orig);
#endif
}


void *MEM_malloc(int align, unsigned int n, char *tag)
{
    static int init;  
    mem *m;
    mem_tail *mt;
    void *p = NULL, *orig = NULL;
    unsigned int size;
    int zeroFlag = 0, memType = 0;  
    // Setup mem manager
    if (init == 0) {
        g_sizeofMem = sizeof(mem);
        init++;
    }
    // Alignment less then 0 means memset to 0 below
    if (align < 0) {
        align = -align;
        zeroFlag++;
    }
    // Set the size of memory needed
    // Round up the request size by 8 to make sure it is not an odd size.
    size = ROUND_UP(8,n) + g_sizeofMem + sizeof(mem_tail); // Rounded(8,Requested) + mem struct + tail struct
    // Check if strange align value asked for
    if (align > MAX_ALIGN)
        align = MAX_ALIGN;
    // If the alignment is not 8 it must be added to the size for rounding.
    if (align != 8)
        size += align;
    // Get the memory from native allocator and check
    orig = p = getmem(size, &memType);  // Yes this ends up being m->size for Linux kmalloc issues
    if (p == NULL) {
        LOG_Log(LOG_ERROR,"jbbmem malloc failed size:%d tag:%s memType:%s\n",size,tag,MemType2str[memType]);
        MEM_Walk("MEM_malloc");
        return p;
    }
    // Do memset if asked for 
    if (p && zeroFlag) 
        memset(p,0,size);
    // Make room for the memory manager structure and align if needed
    p = (void *)((long)p + (long)g_sizeofMem);
    if (align != 8) {
        p = (void *)(((long)p+(long)(align-1)) & ~(long)(align-1L)); /* round up to nearest 8 byte align */       
    }
    // Set memory manager structure values
    m = (void *)((long)p - (long)g_sizeofMem); // Put the mem struct right up against the return pointer.
    m->memStart = MEM_START_TAG;
    m->memHeadTag = MEM_HEAD_TAG;
    m->requestSize = n;
    m->size = size;
    m->tag = *(int *)tag;
    m->align = align;
    m->orig = orig;  
    m->memType = memType;
    // Add a tail memory structure to check for data overruns during free
    mt = (void *)((long)p + (long)(ROUND_UP(8,(long)m->requestSize)));
    mt->memTailTag = MEM_TAIL_TAG;
    // Add this one to the list of allocated memory
    MEM_sem(SEM_LOCK);
    g_jbbStats.mem_inuse += size;
    list_add(&m->list, &Mem_Head);
    MEM_sem(SEM_UNLOCK);
    TRACE_Log(JBBMEM_DBG,"MEM_malloc\tp:%p size:%d tag:%s used:%d sizeofMem:%d orig:%p\n",
        p,m->size,tag,g_jbbStats.mem_inuse,g_sizeofMem,orig);         
    return p;
}

static int free_up(void *pp, int lockFlag) 
{
    int size = 0;
    if (pp != NULL) {
        mem *m = NULL;
        mem_tail *mt = NULL;
        void *p = pp;
        // Search for the beginning of the memory structure.
        m = (void *)((long)p - (long)g_sizeofMem); // This is where it was put so it better be here.
        if (m->memHeadTag != MEM_HEAD_TAG) {
            LOG_Log(LOG_ERROR,"sanmem_free could not locate the proper memory: 0x%x\n",pp);
            return -1;
        }
        // Check for data overrun at the end.
        mt = (void *)((long)p + (long)(ROUND_UP(8,(long)m->requestSize)));
        if (mt->memTailTag != MEM_TAIL_TAG) {
            LOG_Log(LOG_ERROR,"Memory Corruption: End of memory was over written!! tag:%s\n",tag((char *)&m->tag));
        }
        //  Clear the tags from memory. Don't want to find them by mistake.
        m->memHeadTag = 0; 
        mt->memTailTag = 0;

        TRACE_Log(JBBMEM_DBG,"MEM_free\tp:%p size:%d tag:%s used:%d orig:%p\n",
            p,m->size,tag((char *)&m->tag),g_jbbStats.mem_inuse,m->orig);
        // Remove this memory from the list.
        if (lockFlag) MEM_sem(SEM_LOCK);
        g_jbbStats.mem_inuse -= m->size;
        size = m->requestSize;
        list_del(&m->list);
        if (lockFlag) MEM_sem(SEM_UNLOCK);
        memset(pp,'B',m->requestSize);
        // Call the system free command.
        freemem(m);
    }
    return size;
}

int MEM_free(void *pp)
{
    return free_up(pp,1);
}

void MEM_Walk(char *which) 
{
    mem *mem_item = NULL, *next = NULL;
    LOG_Log(LOG_PRINT,"**************** MEM_walk Start ****************%s\n","");
    LOG_Log(LOG_PRINT,"MEM_walk total memory:%d\n",g_jbbStats.mem_inuse);
    MEM_sem(SEM_LOCK);
    if(!list_empty(&Mem_Head)){
        list_for_each_entry_safe(mem_item, next, &Mem_Head, list) {
            LOG_Log(LOG_PRINT,"mem_current:%p size:%d tag:%s align:%d type:%s\n",
                mem_item,
                mem_item->size,
                tag((char *)&mem_item->tag),
                mem_item->align,
                MemType2str[mem_item->memType]);
        }
    }
    MEM_sem(SEM_UNLOCK);
    LOG_Log(LOG_PRINT,"**************** MEM_walk Complete ****************\n","");
}


void MEM_destroy() 
{
    mem *mem_item = NULL, *next = NULL;
    LOG_Log(LOG_PRINT,"MEM_destroy Start\n","");
    LOG_Log(LOG_PRINT,"MEM_destroy total memory:%d\n",g_jbbStats.mem_inuse);
    MEM_sem(SEM_LOCK);
    if(!list_empty(&Mem_Head)){
        list_for_each_entry_safe(mem_item, next, &Mem_Head, list) {
            LOG_Log(LOG_PRINT,"MEM_free\tp:%p size:%d tag:%s used:%d orig:%p type:%s\n",
                mem_item,
                mem_item->size,
                tag((char *)&mem_item->tag),
                g_jbbStats.mem_inuse,
                mem_item->orig,
                MemType2str[mem_item->memType]);
            free_up((void *)((long)mem_item + (long)g_sizeofMem), 0); // Offset memory for free
        }
    }
    MEM_sem(SEM_UNLOCK);
    LOG_Log(LOG_PRINT,"MEM_destroy Complete\n","");
}


/**************************************************
Free-up any items that are currently in an allocated state.
**************************************************/
void MEM_FreeAll(void)
{
    mem *mem_item = NULL, *next = NULL;
    //LOG_Log(LOG_PRINT,"MEM_FreeAll Start\n");
    MEM_sem(SEM_LOCK);
    if(!list_empty(&Mem_Head)){
        list_for_each_entry_safe(mem_item, next, &Mem_Head, list) {
            LOG_Log(LOG_PRINT,"MEM_free*\tp:%p size:%d tag:%s used:%d orig:%p\n",
                mem_item,mem_item->size,tag((char *)&mem_item->tag),g_jbbStats.mem_inuse,mem_item->orig);
            free_up((void *)((long)mem_item + (long)g_sizeofMem), 0); // Offset memory for free
        }
    }
    MEM_sem(SEM_UNLOCK);
}


void MEM_Stats(PF32LONG *inuse, PF32LONG *isfree)
{
    *inuse = g_jbbStats.mem_inuse;
    *isfree = 0;
}


#ifdef MAIN 
int main(int argc, char **argv) 
{
    char *a, *b, *c, *d;

    a = (char *)MEM_malloc(8, 128, "aaaa");        
    b = (char *)MEM_malloc(-8, 1024, "bbbb");        
    c = (char *)MEM_malloc(512, 1400, "cccc");        
    d = (char *)MEM_malloc(8, 1200, "dddd");        

    MEM_Walk("one");

    MEM_free(a);
    //MEM_free(b);

    MEM_Walk("two");

    MEM_free(c);
    //MEM_free(d);

    MEM_destroy();

    MEM_Walk("three");

    return 0;
}
#endif



