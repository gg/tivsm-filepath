/* str.c -- Undefined sting functions for early versions.
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include <linux/types.h>  /* size_t */


#undef strchr
char * strchr(const char * s, int c)
{
    for(; *s != (char) c; ++s)
        if (*s == '\0')
            return NULL;
    return (char *) s;
}

#undef strlen
size_t strlen(const char * s)
{
    const char *sc;

    for (sc = s; *sc != '\0'; ++sc)
        ;
    return sc - s;
}

#undef strcmp
int strcmp(const char * cs,const char * ct)
{
    register signed char res;

    while (1) {
        if ((res = *cs - *ct++) != 0 || !*cs++)
            break;
    }
    return res;
}

#undef strncpy
char * strncpy(char * dest,const char *src,size_t count)
{
    char *tmp = dest;

    while (count-- && (*dest++ = *src++) != '\0')
        ;
    return tmp;
}

#undef strcat
char * strcat(char * dest, const char * src)
{
    char *tmp = dest;

    while (*dest)
        dest++;
    while ((*dest++ = *src++) != '\0')
        ;
    return tmp;
}

#undef strcpy
char * strcpy(char * dest,const char *src)
{
    char *tmp = dest;

    while ((*dest++ = *src++) != '\0')
        ;
    return tmp;
}

#undef strncmp
int strncmp(const char *s1, const char *s2, size_t n)
{
    if (n == 0)
        return (0);
    do {
        if (*s1 != *s2++)
            return (*(const unsigned char *)s1 -
            *(const unsigned char *)(s2 - 1));
        if (*s1++ == 0)
            break;
    } while (--n != 0);
    return (0);
}

#undef strrchr
char *strrchr(const char *s, int c)
{
    char *rtn = 0;

    do {
        if (*s == c)
            rtn = (char*) s;
    } while (*s++);
    return (rtn);
}


static char upcase(char c) {
    if (c>='a' && c<='z') c=(c-'a')+'A';
    return c;
}

#undef strcasecmp
int strcasecmp(char *s1, char *s2) {
    int c1, c2;

    while (*s1) {
        int x;
        if (!*s2) return 1;
        c1 = upcase(*s1);
        c2 = upcase(*s2);
        x=c1-c2;
        if (x) return x;
        s1++;
        s2++;
    }
    if (*s2==0)
        return 0; /* Fully matched */
    else return -1;
}

#undef strncasecmp
int strncasecmp(char *s1, char *s2, size_t n) {
    int c1, c2;

    while (*s1 && n) {
        int x;
        if (!*s2) return 1;
        c1 = upcase(*s1);
        c2 = upcase(*s2);
        x=c1-c2;
        if (x) return x;
        s1++;
        s2++;
        n--;
    }
    if (n) return -1;
    return 0; /* Fully matched */
}
