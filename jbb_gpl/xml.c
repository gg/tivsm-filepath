/* xml.c -- xml handling
Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "jbb.h"

#define SWAP(a, b) {     \
    char c;              \
    c=a; a=b; b=c;       \
  }

static void trim(char *p) {
    int i=strlen(p)-1;
    while (i>0) {
        if (p[i]>' ') break;
        p[i]=0;
        i--;
    }
}

static void doAmpersands(char *output, int max) {
    STR_substitute(output, max, "&apos;", "'");
    STR_substitute(output, max, "&quot;",  "\"");
    STR_substitute(output, max, "&amp;", "&");
    STR_substitute(output, max, "&lt;", "<");
    STR_substitute(output, max, "&gt;", ">");;
}

/*
* Given something like:
*     "<tag item=value item2="value2 v22" item3 item4=value4>" 
* this function  will extract the value associated with the 
* specified item. Returns 0 if used the defalt supplied.
* WARNING: The input string is altered by this function (it
* secretly marks the processed items so they aren't re-processed).
* Yes default is spelled default but not here!!!
*/
char *xml_getAssignment(char *line, char *what, char *output, int max, char *defalt) {
    int len=strlen(what);
    int LooksLikeAFilename=0;
    int copySize;

    *output = 0;

    while (*line) {
        while (*line == '<' || (*line<=' ' && *line)) line++;
        if (!*line) break;
        if (STR_strncasecmp(line,what,len)==0) {
            if (line[len]=='=') {
                char *p,match=' ',inverse='\'';
                int siz,quoted=0,depth=0;

                memset(line,' ',len+1); /* tag as consumed */
                line+=len+1;
                p = line;
                if (*p=='"' || *p=='\'') {
                    match = *p; 
                    if (match == '\'') inverse='\"';
                    p = ++line;  /* Deal with double or single quoted */
                    quoted = 1;
                }

                /* allow alternating quote styles internally; very confusing */
                while (1) {
                    if (!*p) break;  // eol...break out
                    if (*p=='\'' && LooksLikeAFilename) {
                        // Just advance and capture the quote mark...
                        // But only within the first few characters
                    }
                    else
                        if (match == ' ' && ((*p == '/' && p[1]=='>') || *p == '>')) break; // improper, but let it slide
                        else {
                            if ((*p==':' || *p==DIRSLASH) && p-line<4) LooksLikeAFilename=1; // Major hack to deal with single quotes in filenames
                            if (*p == match) {
                                if (match == ' ') break;  // non-quoted case
                                if (depth == 0) break;
                                depth--;
                                SWAP(match,inverse);
                                //printf("found match at [%s], depth now:%d match now [%c] and inverse [%c]\n",p,depth,match,inverse);
                            }
                            else if (*p == inverse) {
                                depth++;
                                SWAP(match,inverse);
                                //printf("found inverse at [%s], depth now:%d match now is [%c] and inverse [%c]\n",p,depth,match,inverse);
                            }
                        }
                        p++;
                }

                siz = p-line;
                copySize = siz;
                if (copySize > max-1) copySize = max-1;
                memcpy(output, line, copySize);
                if (quoted)
                    memset(line-1,' ',siz+2);  /* tag as consume */
                else
                    memset(line,' ',siz);  /* tag as consume */
                output[copySize]=0;
                if (!quoted) trim(output);
                doAmpersands(output, max);
                return output;
            }
            else 
                if (line[len]<=' ' ||
                    line[len]=='/' ||
                    line[len]=='>') {
                        /* Is-present style; that is, no value, only the identifier */
                        memset(line,' ',len);  /* tag as consumed */
                        if (max>0) *output = 0;
                        trim(output);
                        doAmpersands(output, max);
                        return output;
                }
                else /* partial match, like "Thre" of "Three" */
                    while (*line && *line!=' ') line++;
        }
        else {
            while (*line && *line!=' ') line++;
        }
    }
    strncpy(output, defalt, max);
    trim(output);
    doAmpersands(output, max);
    return 0;
}



/*
* Similar to "xml_getAssignment()", but assumes the needed 
* data is a number and the caller prefers the binary rather
* than ascii.
* WARNING: This has no detection nor handling of illegally
* composed numerics (which means something like "val=45a5" would
* evaluate to simply 0).
* Yes default is spelled default but not here!!!
*/
PF32LONG xml_getAssignment32(char *line, char *what, char *defalt) {
    char temp[20];
    PF32LONG x=0;
    if (xml_getAssignment(line, what, temp, sizeof(temp), defalt) && !temp[0])
        x=1;
    else
        x=STR_tol(temp); /* str 'to' 'long' */
    return x;
}


/*
* Determines if the word 'key' is indicated in the
* specified line.  So, line might be: "<rule-list x y z>"
* and key might be "rule-list".  We're leanient and will
* 'line' to *not* start with '<' since it might come in
* from the command line...
*/
int xml_isKeyword(char *key, char *line) {
    int len;    
    if (*line == '<') line++;
    if (*key == '<') key++;
    len=strlen(key);
    if (STR_strncasecmp(line,key,len)==0) {
        if (line[len]==0 ||
            line[len]==' ' ||
            line[len]=='/' ||
            line[len]=='>') return 1;
    }
    return 0;
}

