/* hook.c -- Main module facility file

Licensed under BSD/GPL v2

    BSD
    Copyright (c) 2011, IBM Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the IBM nor the names of its contributors 
      may be used to endorse or promote products derived from this software 
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.

    GPL v2
    Copyright (C) 2011 IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


hook_xxxx   - Functions that overload normal vfs fuctions.  Normal vfs functions are always called.
driver_xxxx - Module specific functions such as open, close and ioctl.

*/

#ifndef __KERNEL__
#  define __KERNEL__
#endif
#ifndef MODULE
#  define MODULE
#endif

#define __NO_VERSION__ /* don't define kernel_verion in module.h */
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/version.h>

#include <linux/kernel.h> /* printk() */
#include <linux/fs.h>     /* everything... */
#include <linux/errno.h>  /* error codes */
#include <linux/types.h>  /* size_t */
#include <linux/fcntl.h>  /* O_ACCMODE */
#include <linux/hdreg.h>  /* HDIO_GETGEO */
#include <linux/file.h>   /* fget... */
#include <linux/mount.h>  /* vfsmount */
#include <linux/list.h>   /* list_head */
#include <linux/limits.h> /* #define PATH_MAX        4096 */
#include <linux/rwsem.h>  /* rw_semaphore */
#include <linux/sched.h>  /* current and schedule */

#if ( LINUX_VERSION_CODE > KERNEL_VERSION(2,6,24) ) // 2.6.25 and above
#include <linux/path.h>  /* Needed for d_path in getFullname */
#include <linux/fs_struct.h>  /* for current->fs */
#endif

#if ( LINUX_VERSION_CODE < KERNEL_VERSION(3,5,0) ) 
#include <asm/system.h>   /* cli(), *_flags */
#endif
#include <asm/uaccess.h>  /* VERIFY_WRITE */
#include <asm/io.h>

# if ( LINUX_VERSION_CODE > KERNEL_VERSION(3,19,0) )
#include <linux/fs_pin.h>
#endif

#include "jbb.h"

MODULE_LICENSE("GPL");

#define DEVICE_NAME "filepath"            /* name for messaging */

struct rw_semaphore hook_sem;

static int hook_release(struct inode *inode, struct file *filp); /* forward */

typedef enum {EXCEPT_CLOSE, EVEN_CLOSE} unhook_mode_e;

static int jbb_major = 0;
static int unload_in_progress = 0;

static int jbbMaxBuffer = 64;
static int jbbAllowWaitIfFull = 1;
module_param(jbbMaxBuffer, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(jbbMaxBuffer, "Sets the maximum buffer size in MB. Default 64MB");
module_param(jbbAllowWaitIfFull, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(jbbAllowWaitIfFull, "Allows a lock when the buffer is full. Default 1");

#if ( LINUX_VERSION_CODE >= KERNEL_VERSION(3,3,0) )

# if defined _MOUNT_H_FOUND  /*=== The Makefile checks if fs/mount.h exists ===*/
#  include <../fs/mount.h>
# else /*=== if not found use this version of struct mount ===*/
/*=== 
  mnt_list was moved from struct vfsmount in linux/mount.h to struct mount in fs/mount.h 
  in 3.3 and is only available in the full source distro.  
===*/
struct mount {
	struct list_head mnt_hash;
	struct mount *mnt_parent;
	struct dentry *mnt_mountpoint;
	struct vfsmount mnt;
#if ( LINUX_VERSION_CODE >= KERNEL_VERSION(3,13,0) )
# if ( LINUX_VERSION_CODE >= KERNEL_VERSION(3,18,0) )
    union {
      struct rcu_head mnt_rcu;
      struct llist_node mnt_llist;
   };
# else
   struct rcu_head mnt_rcu;
# endif
#endif
#ifdef CONFIG_SMP
	struct mnt_pcp __percpu *mnt_pcp;
#else
	int mnt_count;
	int mnt_writers;
#endif
	struct list_head mnt_mounts;	
	struct list_head mnt_child;	
	struct list_head mnt_instance;	
	const char *mnt_devname;
	struct list_head mnt_list;
	struct list_head mnt_expire;	
	struct list_head mnt_share;	
	struct list_head mnt_slave_list;
	struct list_head mnt_slave;
	struct mount *mnt_master;
	struct mnt_namespace *mnt_ns;
	struct mountpoint *mnt_mp;
#if ( LINUX_VERSION_CODE >= KERNEL_VERSION(3,18,0) )
   struct hlist_node mnt_mp_list;
#endif
#ifdef CONFIG_FSNOTIFY
	struct hlist_head mnt_fsnotify_marks;
	__u32 mnt_fsnotify_mask;
#endif
	int mnt_id;			
	int mnt_group_id;		
	int mnt_expiry_mark;		
#if ( LINUX_VERSION_CODE <= KERNEL_VERSION(3,16,0) )
	int mnt_pinned;
#else
   struct hlist_head mnt_pins;
#endif
#if ( LINUX_VERSION_CODE <= KERNEL_VERSION(3,13,0) )
	int mnt_ghosts;
#else
# if ( LINUX_VERSION_CODE <= KERNEL_VERSION(3,19,0) )
   struct path mnt_ex_mountpoint;
# else
   struct fs_pin mnt_umount;
   struct dentry *mnt_ex_mountpoint;
# endif
#endif
};
# endif /*=== _MOUNT_H_FOUND ===*/
#endif

/*
* The following data structure holds onto the original vop
* stuff so that we can restore it when we unload, plus, so that
* we can look it up when we chain to the orig functions.
*
* On linux, we can't simply re-vector a dispatch pointer...sadly.
* But, we can find the dispatch pointer, and revector all the functions
* there.
*/
typedef struct {
    /* First, hold the address of the real/original
    structure in memory */
    struct file_operations  *file_ops_p;
    /* Next, copy what was there to here */
    struct file_operations file_ops;
    /* Repeat for the inode ops */
    struct inode_operations *inode_ops_p;
    struct inode_operations inode_ops;
} LinuxSavedOps_t;

/*=== 
*  rename2 was addopted early (3.10) by some distros and defined in a 
*  special wrapper inode operation structure.  It appears to be 
*  added to the main stream in 3.15.    
===*/
int (*save_rename2)(struct inode *srcI, struct dentry *srcD, struct inode *dstI, struct dentry *dstD, unsigned int flags) = NULL;
#ifdef IS_IOPS_WRAPPER
struct inode_operations_wrapper *save_iop_wrapper;
#else
/*=== Define a fake struct ===*/
struct inode_operations_wrapper {
   void *rename2;
};
struct inode_operations_wrapper *save_iop_wrapper;
#endif

static HASH_ID fops_hash;

/*
We'll put the above structure in to a hash table that
is indexed by both the orig fops and iops so that at
run-time we can get back to this structure from any
hook function and thus get to the orig functions.
*/

static HASH_ID hash_id;

/****************************************************************************************************/
/******* General Functions - Kernel version specific ************************************************/
/****************************************************************************************************/
# if ( LINUX_VERSION_CODE >= KERNEL_VERSION(3,3,0) )

static struct mount *find_mount(struct vfsmount *mnt)
{
	return container_of(mnt, struct mount, mnt);
}

/*=== Called from module init to check if the mount structure matches filepath version ===*/
static int mountStructVerify(void)
{
    int valid = _FALSE;
    struct vfsmount *v = NULL;
    struct path root_path;
    struct mount *mnt = NULL;

    if (current && current->fs) {
        spin_lock(&current->fs->lock);
        root_path = current->fs->root;
        path_get(&root_path);
        v = (struct vfsmount *)(root_path.mnt);
        spin_unlock(&current->fs->lock);

        mnt = find_mount(v);
        if (mnt && mnt->mnt_devname)
        {
           if (strstr(mnt->mnt_devname,"/dev") != NULL)
           {
              valid = _TRUE;
           }
        }
        path_put(&root_path);
    }
    return valid;
}

static char *getfullname(char *out, int max, struct dentry *d, struct vfsmount *vfsmnt, char *who){
    char *res = NULL;
    struct path p;
    if (current && current->fs) {  // Make sure we have a good fs_struct before calling d_path
        if (!vfsmnt) {    
            LOG_Log(LOG_ERROR,"getfullname:  Bad vfsmnt value\n","");
            return "BADVFSMNT";
        }
        p.mnt = vfsmnt;
        p.dentry = d;
        res = d_path( &p, out, max );
        if (IS_ERR(res)) {
            LOG_Log(LOG_ERROR,"getfullname: d_path failed\n","");
            return "BADVFSMNT";
        }
        else {
            /*=== LOG_Log(LOG_INFO,"getfullname: caller %s res %p res [%s]\n",who,res,res); ===*/
        }
    }
    return res;
}

static struct vfsmount *find_vfsmount(struct super_block *sb, char *func) {
    int putNeeded = 0;
    struct vfsmount *v = NULL;
    struct path root_path;
    struct list_head *p = NULL;

    if (current && current->fs) {
        spin_lock(&current->fs->lock);
        root_path = current->fs->root;
        path_get(&root_path);
        putNeeded = 1;
        v = (struct vfsmount *)(root_path.mnt);
        spin_unlock(&current->fs->lock);
    }
    /*=== We have a v for the root mount point.  It may not be the right one for the 
    mount point we are asking for so look through the list of mounts and check if 
    there is a better one.  ===*/
    if (v) {
        struct mount *mnt = find_mount(v);  // Need a place to start the search
        for (p = ((struct mount *)mnt)->mnt_list.next; 
            p && p != &((struct mount *)mnt)->mnt_list; 
            p = p->next) 
        {
            struct mount *tmp_mount = NULL;
            tmp_mount = list_entry(p, struct mount, mnt_list);
            if (tmp_mount && tmp_mount->mnt.mnt_sb == sb) {
                /*=== LOG_Log(LOG_INFO,"find_vfsmount: tmp_mount %p %s \n",&tmp_mount->mnt, func); ===*/
                v=&tmp_mount->mnt;
                break;
            }
        }
    }
    else {
        LOG_Log(LOG_ERROR,"find_vfsmount: Failed to get a name in func:%s\n",func);
    }
    if (putNeeded) path_put(&root_path);
    return v;
}

# elif ( LINUX_VERSION_CODE > KERNEL_VERSION(2,6,24) && LINUX_VERSION_CODE < KERNEL_VERSION(3,3,0)) // 2.6.25 - 3.2.0

static char *getfullname(char *out, int max, struct dentry *d, struct vfsmount *vfsmnt, char *who){
    char *res;
    struct path p;
    if (!vfsmnt) {    
        LOG_Log(LOG_ERROR,"getfullname:  Bad vfsmnt value\n","");
        return "BADVFSMNT";
    }
    p.mnt = vfsmnt;
    p.dentry = d;
    res = d_path( &p, out, max );
    if (IS_ERR(res)) {
        LOG_Log(LOG_ERROR,"getfullname: d_path failed\n","");
        return "BADVFSMNT";
    }
    return res;
}

static struct vfsmount *find_vfsmount(struct super_block *sb, char *func) {
    int putNeeded = 0;
    struct vfsmount *v = NULL;
    struct path root_path;
    struct list_head *p = NULL;

    if (current && current->fs) {
# if ( LINUX_VERSION_CODE > KERNEL_VERSION(2,6,35) ) // 2.6.35 and above
        spin_lock(&current->fs->lock);
#else
        read_lock(&current->fs->lock);
#endif
        root_path = current->fs->root;
        path_get(&root_path);
        putNeeded = 1;
        v = (struct vfsmount *)(root_path.mnt);
# if ( LINUX_VERSION_CODE > KERNEL_VERSION(2,6,35) ) // 2.6.35 and above
        spin_unlock(&current->fs->lock);
#else
        read_unlock(&current->fs->lock);
#endif
    }
    if (v) {
        for (p = ((struct vfsmount *)v)->mnt_list.next; 
            p && p != &((struct vfsmount *)v)->mnt_list; 
            p = p->next) 
        {
            struct vfsmount *tmp_vfsmount;
            tmp_vfsmount = list_entry(p, struct vfsmount, mnt_list);
            if (tmp_vfsmount && tmp_vfsmount->mnt_sb == sb) 
            {
                v=tmp_vfsmount;
                break;
            }
        }
    }
    else {
        LOG_Log(LOG_ERROR,"find_vfsmount: failed to get a name in func:%s\n",func);
    }
    if (putNeeded) path_put(&root_path);
    return v;
}


#else // 2.6.24 and below

static char *getfullname(char *out, int max, struct dentry *d, struct vfsmount *vfsmnt, char *who){
    char *res;
    if (!vfsmnt) {    
        LOG_Log(LOG_ERROR,"getfullname:  Bad vfsmnt value\n","");
        return "BADVFSMNT";
    }
    res = d_path( d, vfsmnt, out, max );
    if (IS_ERR(res)) {
        LOG_Log(LOG_ERROR,"getfullname: d_path failed\n","");
        return "BADVFSMNT";
    }
    return res;
}

static struct vfsmount *find_vfsmount(struct super_block *sb, char *func) {
    struct vfsmount *v=0;
    struct list_head *p;

    if (current && current->fs) {
        v = current->fs->rootmnt;
    }
    if (v) {
        for (p = ((struct vfsmount *)v)->mnt_list.next; 
            p != &((struct vfsmount *)v)->mnt_list; 
            p = p->next) 
        {
            struct vfsmount *tmp_vfsmount;
            tmp_vfsmount = list_entry(p, struct vfsmount, mnt_list);
            if (tmp_vfsmount && tmp_vfsmount->mnt_sb == sb) 
            {
                v=tmp_vfsmount;
                break;
            }
        }
    }
    else {
        LOG_Log(LOG_ERROR,"find_vfsmount: failed to get a name in func:%s\n",func);
    }
    return v;
}

#endif


/****************************************************************************************************/
/******* General Functions **************************************************************************/
/****************************************************************************************************/

// x86 Bit 16 of cr0 determines if the CPU can write to read-only pages http://en.wikipedia.org/wiki/Control_register#CR0
#if defined _x86_64 && LINUX_VERSION_CODE > KERNEL_VERSION(2,6,35) 
#define X86_CR0_PAGE_WP 0x00010000

static void make_page_writable(void) {
    unsigned long cr0 = read_cr0();
    TRACE_Log(TRACE_IOCTL,"make_page_writable: cr0:%x  %s\n",cr0,!(cr0 & X86_CR0_PAGE_WP)?"writable":"read-only");
    if(!(cr0 & X86_CR0_PAGE_WP))
        return;
    write_cr0(cr0 & ~X86_CR0_PAGE_WP);
}

static void make_page_readonly(void) {
    unsigned long cr0 = read_cr0();
    TRACE_Log(TRACE_IOCTL,"make_page_readonly: cr0:%x  %s\n",cr0,cr0 & X86_CR0_PAGE_WP?"read-only":"writable");
    if((cr0 & X86_CR0_PAGE_WP))
        return;
    write_cr0(cr0 | X86_CR0_PAGE_WP);
}
#else

static void make_page_writable(void) {;}

static void make_page_readonly(void) {;}

#endif


static void unhook_fs(unhook_mode_e cl) {
    LinuxSavedOps_t *ops;
    int i;

    down_write(&hook_sem);
    TRACE_Log(TRACE_DEBUG1,"unhook: mode %d\n", cl);
    make_page_writable();
    if (save_rename2 != NULL)
    {
       save_iop_wrapper->rename2 = save_rename2;
    }
    for (i=1; (ops=HASH_FindByIdx(fops_hash, i, 0)); i++) {
        if (ops->file_ops_p) {
            struct file_operations temp;
            temp = ops->file_ops; /* Capture all the origs */
            if (cl==EXCEPT_CLOSE)
                temp.release = hook_release; /* Re-set the close one */
            *ops->file_ops_p = temp; /* Blast them all in */
            if (cl==EVEN_CLOSE)
                ops->file_ops_p = NULL;
        }
        if (ops->inode_ops_p) {
            *ops->inode_ops_p = ops->inode_ops;
            ops->inode_ops_p = NULL;
        }
    }
    make_page_readonly();
    up_write(&hook_sem);
}

#define f_dentry f_path.dentry /* formerly in <linux/fs.h> */

static struct file_operations *get_fops(struct file *filp, char *from) {
    struct file_operations *fops=0;
    struct inode *tinode;
    LinuxSavedOps_t *saved;

    if (filp && filp->f_dentry) {
        tinode = filp->f_dentry->d_inode;
        saved=HASH_Find(fops_hash, (void *)tinode->i_fop, 0);
        if (saved) fops = &saved->file_ops;
    }
    if (!fops) LOG_Log(LOG_ERROR,"hook.c: get_fops: failed to find for filp:%p from %s\n",filp, from);
    return fops;
}


static struct inode_operations *get_iops(struct inode *inode, char *from) {
    struct inode_operations *iops=0;
    LinuxSavedOps_t *saved;

    if (inode) {
        saved=HASH_Find(fops_hash, (void *)inode->i_op, 0);
        if (saved) iops = &saved->inode_ops;
    }
    if (!iops) LOG_Log(LOG_ERROR,"hook.c: get_iops: failed to find for inode:%p from %s\n",inode,from);
    return iops;
}


static char *alloc_fname(void) {
    char *p=MEM_malloc(-8,PATH_MAX,"allf");
    if (!p) LOG_Log(LOG_ERROR,"hook: failed to allocate for filename\n","");
    return p;
}


static void free_fname(char *f) { 
    MEM_free(f); 
}


#if ( LINUX_VERSION_CODE > KERNEL_VERSION(2,6,18) )
# define P_VFSMOUNT filp->f_path.mnt
#else
# define P_VFSMOUNT filp->f_vfsmnt
#endif


static struct inode_operations_wrapper *findIopWrapperAddress(struct inode *inode)
{
	struct inode_operations_wrapper *wrapper = NULL;
		
#ifdef IS_IOPS_WRAPPER
	if (!IS_IOPS_WRAPPER(inode))
		return NULL;
	wrapper = container_of(inode->i_op, struct inode_operations_wrapper, ops);
#endif
	return wrapper;
}

/****************************************************************************************************/
/******* Hook Functions *****************************************************************************/
/****************************************************************************************************/

static int hook_open(struct inode *inode, struct file *filp) {
    int res = 0;
    struct file_operations *orig_file_ops = NULL;
    char *fullname = NULL, *buffer = NULL;

    down_read(&hook_sem);
    orig_file_ops = get_fops(filp,"open");
    if (orig_file_ops->open)
        res=orig_file_ops->open(inode, filp);
    up_read(&hook_sem);

    if (res >= 0) {
        buffer = alloc_fname();
        if (buffer) {
            fullname=getfullname(buffer, PATH_MAX, filp->f_dentry, P_VFSMOUNT /*filp->f_path.mnt*/,"open");
            TRACE_Log(TRACE_OPEN,"hook_open: name:[%s] inode:%p filp:%p dir:%d\n",fullname,inode,filp,S_ISDIR(inode->i_mode));
            if (fullname && 
                jbb_checkExcludeList(fullname) == 0 && 
                jbb_checkWatchList(fullname, S_ISDIR(inode->i_mode)) == 1) {
                    TRACE_Log(TRACE_OPEN,"hook_open: name:[%s] inode:%p filp:%p dir:%d\n",fullname,inode,filp,S_ISDIR(inode->i_mode));
                    IR_Add(fullname, inode, inode->i_mtime.tv_sec, NOT_DIRTY);
            }
            free_fname(buffer);
        }
    }
    return res;
}


#if ( LINUX_VERSION_CODE >= KERNEL_VERSION(3,6,0) ) 
static int hook_create(struct inode *inode, struct dentry *d, umode_t flags, bool b) {
#else
static int hook_create(struct inode *inode, struct dentry *d, int flags, struct nameidata *n) {
#endif
    int res = -1;
    struct inode_operations *orig_inode_ops = NULL; 
    struct vfsmount *v = NULL;
    char *fullname = NULL, *buffer = NULL;

    down_read(&hook_sem);
    orig_inode_ops = get_iops(inode,"create");
    if (orig_inode_ops->create)
#if ( LINUX_VERSION_CODE >= KERNEL_VERSION(3,6,0) ) 
        res=orig_inode_ops->create(inode, d, flags, b);
#else
        res=orig_inode_ops->create(inode, d, flags, n);
#endif
    up_read(&hook_sem);

    if (res >= 0 && d->d_inode) {
        buffer = alloc_fname();
        if (buffer) {
            v=find_vfsmount(inode->i_sb, "create");
            if (v) {
               fullname=getfullname(buffer, PATH_MAX, d, v, "create");
               if (fullname &&
                  jbb_checkExcludeList(fullname) == 0 && 
                  jbb_checkWatchList(fullname,S_ISDIR(d->d_inode->i_mode)) == 1) {
                       TRACE_Log(TRACE_CREATE,"hook_create: name:[%s] inode:%p d->d_inode:%p dir:%d\n",fullname,inode,d->d_inode,S_ISDIR(d->d_inode->i_mode));
                     IR_Add(fullname, d->d_inode, d->d_inode->i_mtime.tv_sec, DIRTY_DATA);  // New file so yes it is created dirty
               }
            }
            free_fname(buffer);
        }
    }

    return res;
}


static int hook_release(struct inode *inode, struct file *filp) {
    int res = -1, dirty = NOT_DIRTY, isDir = 0;
    struct file_operations *orig_file_ops = NULL;
    char *fullname = NULL, *buffer = alloc_fname(), *p = NULL;
    VfsAction_e action = -1;
    PFTIME mtime = 0;

    if (buffer && filp) {
        fullname=getfullname(buffer, PATH_MAX, filp->f_dentry, P_VFSMOUNT, "release");
        if (fullname) {
            p=strstr(fullname," (deleted)");  /* Linux adds this to the name during removal when the inode link is broken */
            if (p) *p=0;
            if (jbb_checkExcludeList(fullname) == 0) {
                TRACE_Log(TRACE_CLOSE,"hook_release: name:[%s] inode:%p filp:%p dir:%d\n",fullname,inode,filp,S_ISDIR(inode->i_mode));
                if (IR_GetDirty(fullname, inode, &dirty, &mtime) == _TRUE) { // Is this a watched file?
                    isDir = S_ISDIR(inode->i_mode);

                    if (dirty == DIRTY_DATA) {
                        TRACE_Log(TRACE_CLOSE,"hook_release: File [%s] data is dirty!! mtime:%d isDir:%d\n",
                                  fullname, inode->i_mtime.tv_sec, isDir);
                        action =  VfsClose_e;
                        mtime = inode->i_mtime.tv_sec;  // Get current mtime
                    }
                    else if (dirty == DIRTY_ATTR) {
                        TRACE_Log(TRACE_CLOSE,"hook_release: File [%s] attributes are dirty!! mtime:%d isDir:%d\n",
                                  fullname, inode->i_mtime.tv_sec, isDir);
                        action = VfsChattr_e;
                        mtime = inode->i_mtime.tv_sec;  // Get current mtime
                    }
                    else if (mtime != inode->i_mtime.tv_sec) {
                        TRACE_Log(TRACE_CLOSE,"hook_release: [%s] Open mtime:%d Close mtime:%d isDir:%d\n",fullname,mtime,inode->i_mtime.tv_sec,isDir);
                        action =  VfsChDate_e;
                        mtime = inode->i_mtime.tv_sec;  // Get current mtime
                    }

                }
            }
        }
    }

    down_read(&hook_sem);
    orig_file_ops = get_fops(filp,"release");
    if (orig_file_ops->release)
        res=orig_file_ops->release(inode, filp);
    up_read(&hook_sem);

    if (res >= 0 && fullname && action != -1)
        jbb_log_data(fullname, NULL, action, mtime, isDir);

    if (buffer) 
        free_fname(buffer);

    return res;
}


static ssize_t hook_write(struct file *filp, const char *buf, size_t s, loff_t *d) {
    ssize_t res = -1;
    struct file_operations *orig_file_ops = NULL;
    char *fullname = NULL, *buffer = NULL;

    down_read(&hook_sem);
    orig_file_ops = get_fops(filp,"write");
    if (orig_file_ops->write)
        res = orig_file_ops->write(filp,buf,s,d);  
    up_read(&hook_sem);

    if (res >= 0) {
        buffer=alloc_fname();
        if (buffer) {
            fullname=getfullname(buffer, PATH_MAX, filp->f_dentry, P_VFSMOUNT, "write");
            if (fullname &&
                jbb_checkExcludeList(fullname) == 0  &&
                jbb_checkWatchList(fullname,S_ISDIR(filp->f_dentry->d_inode->i_mode)) == 1) {
                    TRACE_Log(TRACE_WRITE,"hook_write: name:[%s] filp:%p\n",fullname,filp);
                    IR_SetAttr(fullname, filp->f_dentry->d_inode, 0 /*mtime*/, DIRTY_DATA); // File is being written to so dirty
            }
            free_fname(buffer);
        }
    }

    return res;
}


static int hook_rmdir(struct inode *inode,struct dentry *d) {
    int res = -1, isDir = 0;
    struct inode_operations *orig_inode_ops = NULL;
    struct vfsmount *v = NULL;
    char *fullname = NULL, *buffer = alloc_fname();
    char *p = NULL;
    PFTIME mtime = 0;

    if (buffer) {
        v=find_vfsmount(inode->i_sb, "rmdir");
        if (v) {
            fullname=getfullname(buffer, PATH_MAX, d, v, "rmdir");
            if (fullname) {
                p=strstr(fullname," (deleted)");  /* Linux adds this to the name during removal when the inode link is broken */
                if (p) *p=0;
                if (jbb_checkExcludeList(fullname) == 0) {
                    TRACE_Log(TRACE_DIR,"hook_rmdir: name:[%s] inode:%p dir:%d\n",fullname,inode,S_ISDIR(inode->i_mode));
                    mtime = inode->i_mtime.tv_sec;
                    isDir = S_ISDIR(inode->i_mode);
                }
            }
        }
    }

    down_read(&hook_sem);
    orig_inode_ops = get_iops(inode,"rmdir");
    if (orig_inode_ops->rmdir)
        res=orig_inode_ops->rmdir(inode, d);
    up_read(&hook_sem);

    if (res >= 0 && fullname)
        jbb_log_data(fullname, NULL, VfsRmdir_e, mtime, isDir);

    if (buffer) 
        free_fname(buffer);

    return res;
}


#if ( LINUX_VERSION_CODE >= KERNEL_VERSION(3,3,0) ) 
static int hook_mkdir(struct inode *inode, struct dentry *d, umode_t m) {
#else
static int hook_mkdir(struct inode *inode, struct dentry *d, int m) {
#endif
    int res = -1;
    struct inode_operations *orig_inode_ops = NULL;
    struct vfsmount *v = NULL;
    char *fullname = NULL, *buffer = NULL;

    down_read(&hook_sem);
    orig_inode_ops = get_iops(inode,"mkdir");
    if (orig_inode_ops->mkdir)
        res=orig_inode_ops->mkdir(inode, d, m);
    up_read(&hook_sem);

    if (res >= 0) {
        buffer = alloc_fname();
        if (buffer) {
            v=find_vfsmount(inode->i_sb, "mkdir");
            if (v) {
                fullname=getfullname(buffer, PATH_MAX, d, v, "mkdir");
                if (fullname && jbb_checkExcludeList(fullname) == 0) {
                    TRACE_Log(TRACE_DIR,"hook_mkdir: name:[%s] inode:%p dir:%d\n",fullname,inode,S_ISDIR(inode->i_mode));
                    jbb_log_data(fullname, NULL, VfsMkdir_e, inode->i_mtime.tv_sec, S_ISDIR(inode->i_mode));
                }
            }
            free_fname(buffer);
        }
    }

    return res;
}


static int hook_setattr(struct dentry *d, struct iattr *attr) {
    int res = -1;
    FPRESULT found = _FALSE;
    VfsAction_e vfsAction = -1;
    struct inode *inode = d->d_inode;
    struct inode_operations *orig_inode_ops = NULL;
    struct vfsmount *v = NULL;
    char *fullname = NULL, *buffer = NULL;

    down_read(&hook_sem);
    orig_inode_ops = get_iops(inode,"setattr");
    if (orig_inode_ops->setattr) 
        res=orig_inode_ops->setattr(d, attr);
    up_read(&hook_sem);
    
    if (res >= 0) {
        buffer = alloc_fname();
        if (buffer) {
            v=find_vfsmount(inode->i_sb, "setattr");
            if (v) {
                fullname=getfullname(buffer, PATH_MAX, d, v, "setattr");
                if (fullname && jbb_checkExcludeList(fullname) == 0) {
                    
                    TRACE_Log(TRACE_SETATTR,"hook_setattr: name:[%s] inode:%p dir:%d ia_valid:%o ia_size:%llu\n",
                              fullname, inode, S_ISDIR(inode->i_mode), attr->ia_valid, attr->ia_size);

                    if ((attr->ia_valid & ATTR_SIZE))
                        vfsAction = VfsClose_e; /*=== truncate() operation ===*/ 
                    else if (attr->ia_valid & (ATTR_MODE | ATTR_UID | ATTR_GID))
                        vfsAction = VfsChattr_e;
                    else if ((attr->ia_valid & ATTR_MTIME) || (attr->ia_valid & ATTR_MTIME_SET))
                        vfsAction = VfsChDate_e;

                    if (vfsAction != -1) {
                       found = IR_SetAttr(fullname, inode, inode->i_mtime.tv_sec,
                                                   vfsAction == VfsClose_e ? DIRTY_DATA : DIRTY_ATTR);
                       if (found == _FALSE)
                       {
                          jbb_log_data(fullname, NULL, vfsAction, inode->i_mtime.tv_sec, S_ISDIR(inode->i_mode));
                       }
                    }
                }
            }
            free_fname(buffer);
        }
    }
    return res;
}


static int hook_setxattr(struct dentry *d, const char *name, const void *value, size_t size, int flags)
{
    int res = -1;
    FPRESULT found = _FALSE;
    struct inode *inode = d->d_inode;
    struct inode_operations *orig_inode_ops = NULL;
    struct vfsmount *v = NULL;
    char *fullname = NULL, *buffer = NULL;

    down_read(&hook_sem);
    orig_inode_ops = get_iops(inode,"setxattr");
    if (orig_inode_ops->setxattr) 
        res=orig_inode_ops->setxattr(d, name, value, size, flags);
    up_read(&hook_sem);

    if (res >= 0) {
        buffer = alloc_fname();
        if (buffer) {
            v=find_vfsmount(inode->i_sb, "setxattr");
            if (v) {
                fullname=getfullname(buffer, PATH_MAX, d, v, "setxattr");
                if (fullname && jbb_checkExcludeList(fullname) == 0) {
                    TRACE_Log(TRACE_SETATTR,"hook_setxattr: filename:[%s] name [%s] inode:%p dir:%d\n", fullname, name, inode,S_ISDIR(inode->i_mode));
                    found = IR_SetAttr(fullname, inode, inode->i_mtime.tv_sec, DIRTY_DATA);
                    if (found == _FALSE)
                    {
                       jbb_log_data(fullname, NULL, VfsClose_e, inode->i_mtime.tv_sec, S_ISDIR(inode->i_mode));
                    }
                }
            }
            free_fname(buffer);
        }
    }
    return res;
}


static int hook_rename (struct inode *srcI, struct dentry *srcD,
                        struct inode *dstI, struct dentry *dstD) {
    int res = -1, isDir = 0;
    char *sFullname = NULL, *srcBuffer = NULL;
    char *dFullname = NULL, *dstBuffer = NULL;
    PFTIME mtime = 0;
    struct inode_operations *orig_inode_ops = NULL;
    struct vfsmount *v = NULL;
    struct inode *sInode = srcD->d_inode;

    srcBuffer = alloc_fname();
    if (srcBuffer) {
        dstBuffer = alloc_fname();
        if (dstBuffer) {
            if (sInode) {
                v=find_vfsmount(sInode->i_sb, "src_rename");
                if (v) {
                    sFullname=getfullname(srcBuffer, PATH_MAX, srcD, v, "rename_src");
                    if (sFullname) {
                        v=find_vfsmount(sInode->i_sb, "dst_rename");
                        if (v) {
                            dFullname=getfullname(dstBuffer, PATH_MAX, dstD, v, "rename_dst");
                            if (dFullname) {
                                TRACE_Log(TRACE_RENAME,"hook_rename: sname:[%s] dname:[%s] srcI:%p dstI:%p \n",sFullname,dFullname,srcI,dstI);
                                mtime =  sInode->i_mtime.tv_sec;
                                isDir = S_ISDIR(sInode->i_mode);
                            }
                        }
                    }
                }
            }
            else {
                LOG_Log(LOG_ERROR,"hook_rename: Audit failed: sInode:%p\n",sInode);
            }
        }
    }

    down_read(&hook_sem);
    orig_inode_ops = get_iops(srcI,"rename");
    if (orig_inode_ops->rename) 
        res=orig_inode_ops->rename(srcI, srcD, dstI, dstD);
    up_read(&hook_sem);

    if (res >= 0 && dFullname && sFullname) {
        jbb_log_data(dFullname, NULL, VfsClose_e, mtime, isDir);  // New file close
        jbb_log_data(sFullname, dFullname, VfsRename_e, mtime, isDir); // Old file rename
    }

    if (srcBuffer)
        free_fname(srcBuffer);
    if (dstBuffer)
        free_fname(dstBuffer);

    return res;
}

static int hook_rename2(struct inode *srcI, struct dentry *srcD, 
                        struct inode *dstI, struct dentry *dstD, unsigned int flags) {
    int res = -1, isDir = 0;
    char *sFullname = NULL, *srcBuffer = NULL;
    char *dFullname = NULL, *dstBuffer = NULL;
    PFTIME mtime = 0;
    struct inode_operations *orig_inode_ops = NULL;
    struct vfsmount *v = NULL;
    struct inode *sInode = srcD->d_inode;

    srcBuffer = alloc_fname();
    if (srcBuffer) {
        dstBuffer = alloc_fname();
        if (dstBuffer) {
            if (sInode) {
                v=find_vfsmount(sInode->i_sb, "src_rename2");
                if (v) {
                    sFullname=getfullname(srcBuffer, PATH_MAX, srcD, v, "rename2_src");
                    if (sFullname) {
                        v=find_vfsmount(sInode->i_sb, "dst_rename2");
                        if (v) {
                            dFullname=getfullname(dstBuffer, PATH_MAX, dstD, v, "rename2_dst");
                            if (dFullname) {
                                TRACE_Log(TRACE_RENAME,"hook_rename2: sname:[%s] dname:[%s] srcI:%p dstI:%p \n",sFullname,dFullname,srcI,dstI);
                                mtime =  sInode->i_mtime.tv_sec;
                                isDir = S_ISDIR(sInode->i_mode);
                            }
                        }
                    }
                }
            }
            else {
                LOG_Log(LOG_ERROR,"hook_rename2: Audit failed: sInode:%p\n",sInode);
            }
        }
    }

    down_read(&hook_sem);
    if (save_rename2)
    {
       res = save_rename2(srcI, srcD, dstI, dstD, flags);
    }
    else
    {
       orig_inode_ops = get_iops(srcI,"rename2");
#if ( LINUX_VERSION_CODE >= KERNEL_VERSION(3,15,0) )
       if (orig_inode_ops->rename2) 
          res=orig_inode_ops->rename2(srcI, srcD, dstI, dstD, flags);
#endif
    }
    up_read(&hook_sem);

    if (res >= 0 && dFullname && sFullname) {
        jbb_log_data(dFullname, NULL, VfsClose_e, mtime, isDir);  // New file close
        jbb_log_data(sFullname, dFullname, VfsRename_e, mtime, isDir); // Old file rename
    }

    if (srcBuffer)
        free_fname(srcBuffer);
    if (dstBuffer)
        free_fname(dstBuffer);

    return res;
}


#if ( LINUX_VERSION_CODE >= KERNEL_VERSION(3,3,0) ) 
int hook_mknod(struct inode *inode, struct dentry *d, umode_t mode, dev_t rdev) {
#else
int hook_mknod(struct inode *inode, struct dentry *d, int mode, dev_t rdev) {
#endif
    int res = -1;
    struct inode_operations *orig_inode_ops = NULL;
    struct vfsmount *v = NULL;
    char *fullname = NULL, *buffer = NULL;
    struct inode *file_inode = NULL;

    down_read(&hook_sem);
    orig_inode_ops = get_iops(inode,"mknod");
    if (orig_inode_ops->mknod)
        res = orig_inode_ops->mknod(inode,d,mode,rdev);  
    up_read(&hook_sem);

    if (res >= 0) {
        buffer = alloc_fname();
        if (buffer) {
            if (d){
                file_inode = d->d_inode;  // The inode pointer passed in is for the directory
                if (file_inode) {
                    v=find_vfsmount(file_inode->i_sb, "mknod");
                    if (v) {
                        fullname=getfullname(buffer, PATH_MAX, d, v, "mknod");
                        if (fullname && jbb_checkExcludeList(fullname) == 0) {
                            TRACE_Log(TRACE_MKNOD,"hook_mknod: name:[%s] inode:%p dir:%d\n",fullname,file_inode,S_ISDIR(file_inode->i_mode));
                            jbb_log_data(fullname, NULL, VfsClose_e /*VfsMknod_e*/, file_inode->i_mtime.tv_sec, S_ISDIR(file_inode->i_mode));
                        }
                    }
                }
                else {
                    LOG_Log(LOG_ERROR,"hook_mknod: Audit failed: file_inode:%p\n",file_inode);
                }
            }
            free_fname(buffer);
        }
    }
    return res;
}


static int hook_unlink(struct inode *inode,struct dentry *d) {
    int res = -1, isDir = 0;
    PFTIME mtime = 0;
    struct inode_operations *orig_inode_ops = NULL; 
    struct vfsmount *v = NULL;
    char *fullname = NULL, *buffer = alloc_fname();
    struct inode *file_inode = NULL;

    if (buffer) {
        if (d){
            file_inode = d->d_inode;  // The inode pointer passed in is for the directory
            if (file_inode) {
                v=find_vfsmount(file_inode->i_sb, "unlink");
                if (v) {
                    fullname=getfullname(buffer, PATH_MAX, d, v, "unlink");
                    if (fullname && jbb_checkExcludeList(fullname) == 0) {
                        TRACE_Log(TRACE_UNLINK,"hook_unlink: name:[%s] inode:%p dir:%d\n",fullname,file_inode,S_ISDIR(file_inode->i_mode));
                        mtime = file_inode->i_mtime.tv_sec;
                        isDir = S_ISDIR(file_inode->i_mode);
                    }
                }
            }
            else {
                LOG_Log(LOG_ERROR,"hook_unlink: Audit failed: file_inode:%p\n",file_inode);
            }
        }
    }

    down_read(&hook_sem);
    orig_inode_ops = get_iops(inode,"unlink");
    if (orig_inode_ops->unlink)
        res=orig_inode_ops->unlink(inode, d);
    up_read(&hook_sem);

    if (res >= 0 && fullname)
        jbb_log_data(fullname, NULL, VfsUnlink_e, mtime, isDir);

    if (buffer)
        free_fname(buffer);

    return res;
}


static int hook_link(struct dentry *d1, struct inode *inode, struct dentry *d2) {
    int res = -1;
    struct inode_operations *orig_inode_ops = NULL;
    struct vfsmount *v = NULL;
    char *fullname = NULL, *buffer = NULL;
    struct inode *file_inode = NULL;

    down_read(&hook_sem);
    orig_inode_ops = get_iops(inode,"link");
    if (orig_inode_ops->link)
        res = orig_inode_ops->link(d1,inode,d2);  
    up_read(&hook_sem);

    if (res >= 0) {
        buffer = alloc_fname();
        if (buffer) {
            if (d2){
                file_inode = d2->d_inode;  // The inode pointer passed in is for the directory
                if (file_inode) {
                    v=find_vfsmount(file_inode->i_sb, "link");
                    if (v) {
                        fullname=getfullname(buffer, PATH_MAX, d2, v, "link");
                        if (fullname && jbb_checkExcludeList(fullname) == 0) {
                            TRACE_Log(TRACE_LINK,"hook_link: name:[%s] inode:%p dir:%d\n",fullname,file_inode,S_ISDIR(file_inode->i_mode));
                            jbb_log_data(fullname, NULL, VfsClose_e /*VfsLink_e*/, file_inode->i_mtime.tv_sec, S_ISDIR(file_inode->i_mode));
                        }
                    }
                }
                else {
                    LOG_Log(LOG_ERROR,"hook_link: Audit failed: file_inode:%p\n",file_inode);
                }
            }
            free_fname(buffer);
        }
    }
    return res;
}


static int hook_symlink(struct inode *inode, struct dentry *d, const char *c) {
    int res = -1;
    struct inode_operations *orig_inode_ops = NULL;
    struct vfsmount *v = NULL;
    char *fullname = NULL, *buffer = NULL;
    struct inode *file_inode = NULL;

    down_read(&hook_sem);
    orig_inode_ops = get_iops(inode,"symlink");
    if (orig_inode_ops->symlink)
        res = orig_inode_ops->symlink(inode,d,c);  
    up_read(&hook_sem);

    if (res >= 0) {
        buffer = alloc_fname();
        if (buffer) {
            if (d){
                file_inode = d->d_inode;  // The inode pointer passed in is for the directory
                if (file_inode) {
                    v=find_vfsmount(file_inode->i_sb, "symlink");
                    if (v) {
                        fullname=getfullname(buffer, PATH_MAX, d, v, "symlink");
                        if (fullname && jbb_checkExcludeList(fullname) == 0) {
                            TRACE_Log(TRACE_SYMLINK,"hook_symlink: name:[%s] inode:%p dir:%d c:%s\n",
                                fullname,file_inode,S_ISDIR(file_inode->i_mode),c);
                            jbb_log_data(fullname, NULL, VfsClose_e/*VfsSymlink_e*/, file_inode->i_mtime.tv_sec, S_ISDIR(file_inode->i_mode));
                        }
                    }
                }
                else {
                    LOG_Log(LOG_ERROR,"hook_symlink: Audit failed: file_inode:%p\n",file_inode);
                }
            }
            free_fname(buffer);
        }
    }
    return res;
}

int hook_removexattr(struct dentry *d, const char *name) {
    int res = -1;
    FPRESULT found = _FALSE;
    struct inode *inode = d->d_inode;
    struct inode_operations *orig_inode_ops = NULL;
    struct vfsmount *v = NULL;
    char *fullname = NULL, *buffer = NULL;

    down_read(&hook_sem);
    orig_inode_ops = get_iops(inode,"removexattr");
    if (orig_inode_ops->removexattr) 
        res=orig_inode_ops->removexattr(d, name);
    up_read(&hook_sem);

    if (res >= 0) {
        buffer = alloc_fname();
        if (buffer) {
            v=find_vfsmount(inode->i_sb, "removexattr");
            if (v) {
                fullname=getfullname(buffer, PATH_MAX, d, v, "removexattr");
                if (fullname && jbb_checkExcludeList(fullname) == 0) {
                   TRACE_Log(TRACE_SETATTR,"hook_removexattr: filename:[%s] name [%s] inode:%p dir:%d\n", fullname, name, inode,S_ISDIR(inode->i_mode));
                   found = IR_SetAttr(fullname, inode, inode->i_mtime.tv_sec, DIRTY_DATA);
                   if (found == _FALSE)
                   {
                      jbb_log_data(fullname, NULL, VfsClose_e, inode->i_mtime.tv_sec, S_ISDIR(inode->i_mode));
                   }
                }
            }
            free_fname(buffer);
        }
    }
    return res;
}



/* hook_fs - Replaces the current file and inode operations with hook_* operations */
static int hook_fs(int fd)
{
    int result = 0;
    struct file *filp = NULL;
    struct inode *tinode = NULL;

    filp = fget(fd);
    if (filp == NULL) {
        LOG_Log(LOG_ERROR,"hook_fs: fget returned a NULL file pointer\n","");
        return -EINVAL;
    }
    if (filp->f_dentry == NULL || filp->f_dentry->d_inode == NULL) {
        fput(filp);
        LOG_Log(LOG_ERROR,"hook_fs: f_dentry or fdentry->d_inode is NULL\n","");
        return -EINVAL;
    }
    tinode = filp->f_dentry->d_inode;
    TRACE_Log(TRACE_HOOK,"hook_fs:Hooking; using inode:%p fd:%d\n",tinode,fd);
    if (tinode->i_fop->open == hook_open) {
        /* Do nothing; we're already hooked. */      
        TRACE_Log(TRACE_HOOK, "hook_fs: %s!\n","Already Hooked");
    }
    else {
        LinuxSavedOps_t *saved_ops;
        // to avoid compile errors due to const-ness of i_op and i_fop
        struct file_operations  *i_fop = *((struct file_operations  **) ((void *)tinode + offsetof(struct inode, i_fop)));
        struct inode_operations *i_op  = *((struct inode_operations **) ((void *)tinode + offsetof(struct inode, i_op)));

        saved_ops=MEM_malloc(-8, sizeof(*saved_ops),"ops");
        if (saved_ops) {
            down_write(&hook_sem); 
            //  file operations overloaded
            saved_ops->file_ops_p = (struct file_operations *) tinode->i_fop;
            saved_ops->file_ops   = *tinode->i_fop;
            TRACE_Log(TRACE_HOOK,"hook_fs i_fop->open:%p i_op->setattr:%p\n",i_fop->open,i_op->setattr);

            make_page_writable();
            {
               struct inode_operations_wrapper *iop_wrapper;
               /*=== See if inode_operations_wrapper is defined in this kernel ===*/
               iop_wrapper = findIopWrapperAddress(tinode); 
               if (iop_wrapper != NULL && save_rename2 == NULL)
               {
                  TRACE_Log(TRACE_HOOK,"hook: rename2 %p\n",iop_wrapper->rename2);
                  save_iop_wrapper = iop_wrapper;
                  save_rename2 = iop_wrapper->rename2;
                  iop_wrapper->rename2 = hook_rename2;
               }
            }
            i_fop->open     =  hook_open;
            i_fop->release  =  hook_release;
            i_fop->write    =  hook_write;

            // inode operations - They are not called with a file pointer
            saved_ops->inode_ops_p = (struct inode_operations *) tinode->i_op;
            saved_ops->inode_ops   = *tinode->i_op;
            i_op->setattr    =  hook_setattr;
            i_op->setxattr   =  hook_setxattr; // Posix acl changes
            i_op->rename     =  hook_rename;
#if ( LINUX_VERSION_CODE >= KERNEL_VERSION(3,15,0) ) /*=== Added to mainstream here ===*/
            i_op->rename2    =  hook_rename2;
#endif
            i_op->unlink     =  hook_unlink;
            i_op->rmdir      =  hook_rmdir;
            i_op->mkdir      =  hook_mkdir;
            i_op->create     =  hook_create;
            i_op->mknod      =  hook_mknod;
            i_op->link       =  hook_link;    // hard link
            i_op->symlink    =  hook_symlink; // symbolic (soft) link
            i_op->removexattr = hook_removexattr;

            //  Hash the original pointers so they can be called in the hook functions
            result = HASH_Add(fops_hash, saved_ops->file_ops_p, saved_ops);
            if (result != 0) {
                LOG_Log(LOG_ERROR,"hook_fs: fops_hash add of file_ops failed: %d\n",result);
            }
            else {
                result = HASH_Add(fops_hash, saved_ops->inode_ops_p, saved_ops);
                if (result != 0) {
                    LOG_Log(LOG_ERROR,"hook_fs: fops_hash add of inode_ops failed: %d\n",result);
                }
            }
            if (result != 0) { // Since Hash failed lets put everything back.
                if (saved_ops->file_ops_p) {
                    *saved_ops->file_ops_p = saved_ops->file_ops;
                    saved_ops->file_ops_p = NULL;
                }
                if (saved_ops->inode_ops_p) {
                    *saved_ops->inode_ops_p = saved_ops->inode_ops;
                    saved_ops->inode_ops_p = NULL;
                }
            }
            make_page_readonly();		
            up_write(&hook_sem); 
        }
        else {
            LOG_Log(LOG_ERROR,"hook_fs: fget returned a NULL file pointer\n","");
            result = FP_ERR_NOMEM;
        }
    }
    TRACE_Log(TRACE_DEBUG1, "hook_fs: %s.\n","hooked");
    fput(filp);
    return result;
}

/****************************************************************************************************/
/******** Driver Functions ***************************************************************************/
/****************************************************************************************************/

static int validateCtl(PF_Ctl_t *ctl)
{
    // Called from driver_ioctl - returns 0 Bad 1 Good
    if (ctl == NULL) return 0;
    // If there is data coming in make sure it will fit in the buffer
    if (ctl->nBytesIn > 0 && ctl->nBytesIn > ctl->maxOut) return 0;
    // Can the user's data pointer be accessed for read and write
    if (!access_ok(VERIFY_WRITE, ctl->dataptr, ctl->nBytesIn)) return 0;
    return 1;
}

# if ( LINUX_VERSION_CODE > KERNEL_VERSION(2,6,35) ) 
long driver_ioctl (struct file *fp, unsigned int cmd, unsigned long arg) {
     long result = 0;
#else
int driver_ioctl (struct inode *inode, struct file *fp, unsigned int cmd, unsigned long arg) {
     int result = 0;
#endif
    unsigned long memCopyRes = 0;
    int fd;
    PF_Ctl_t ctl;
    char *buffer = NULL;

    TRACE_Log(TRACE_HOOK,"driver_ioctl: enter cmd:%u\n",cmd);
    if (unload_in_progress == 1) {  // If Filepath has been stopped - do not let any callers through
        LOG_Log(LOG_ERROR,"driver_ioctl: %s is exiting returning EINVAL to the caller %s.\n",DEVICE_NAME,current->comm);
        return -EINVAL;
    }

    /* Get header */
    if (!access_ok(VERIFY_WRITE, arg, sizeof(ctl))) { // Check for read and write access. Write is used later
        TRACE_Log(TRACE_HOOK,"driver_ioctl exit result:%d\n",-EINVAL);
        return -EINVAL;
    }
    memCopyRes = copy_from_user((char *)&ctl, (char *)arg, sizeof(ctl));
    if (memCopyRes != 0) {
        LOG_Log(LOG_ERROR,"driver_ioctl: copy_from_user failed to copy %ul bytes out of %ul\n",
            memCopyRes,sizeof(ctl));
        TRACE_Log(TRACE_HOOK,"driver_ioctl exit result:%d\n",-EINVAL);
        return -EINVAL;
    }

    if (validateCtl(&ctl) == 0) {
        LOG_Log(LOG_ERROR,"driver_ioctl: invalid parameters provided by the caller.\n","");
        TRACE_Log(TRACE_HOOK,"driver_ioctl exit resuilt:%d\n",-EINVAL);
        return -EINVAL;
    }

    if (!ctl.maxOut) {
        LOG_Log(LOG_ERROR,"driver_ioctl: no output space provided by the caller.\n","");
        TRACE_Log(TRACE_HOOK,"driver_ioctl exit resuilt:%d\n",-EINVAL);
        return -EINVAL;
    }

    buffer = MEM_malloc(-8,ctl.maxOut,"fpac");
    if (!buffer) {
        LOG_Log(LOG_ERROR,"driver_ioctl: Could not allocate output buffer of %d bytes.\n",ctl.maxOut);
        TRACE_Log(TRACE_HOOK,"driver_ioctl exit result:%d",-EINVAL);
        return -EINVAL;
    }

    /* Now copy full amount of in-bound data */
    if (ctl.nBytesIn > 0) {
        memCopyRes = copy_from_user((char *)buffer, (char *)ctl.dataptr, ctl.nBytesIn);
        if (memCopyRes != 0) {
            LOG_Log(LOG_ERROR,"driver_ioctl: copy_from_user failed to copy %ul bytes out of %d\n",
                memCopyRes,ctl.nBytesIn);
            MEM_free(buffer);
            TRACE_Log(TRACE_HOOK,"driver_ioctl result:%d",-EINVAL);          
            return -EINVAL;
        }
    }

    TRACE_Log(TRACE_IOCTL,"ioctl cmd:%u sub:%d in:[%s] outsize:%d\n",
        cmd, ctl.subCommand, buffer, ctl.maxOut);

    switch (cmd) {
    case PF_IOCTL_HOOK_DIR:
    case PF_IOCTL_HOOK:
        fd = (int)STR_tol(buffer); /* The fd is in string-form...*/
        result = hook_fs(fd);
        ctl.result = result;
        break;

    case PF_IOCTL_CTL:
        /* The general jbb_ctl handler can handle everything else */
        result = jbb_ctl(&ctl, buffer);
        /* Copy block back to user */
        if (result == FP_INFO_XMLRESPONSE || result == FP_INFO_SUCCESS) {
            memCopyRes = copy_to_user((char *)ctl.dataptr, buffer, ctl.nBytesOut);
            if (memCopyRes != 0) {
                LOG_Log(LOG_ERROR,"driver_ioctl:(2) copy_to_user failed to copy %ul bytes out of %d\n",
                    memCopyRes,ctl.nBytesOut);
                result = -EINVAL;
            }
        }
        /* Announce an error...if it is truly an error */
        if (result && FPERR_getSeverity(result) <= fpErrorLevel_Error_e)
            FPERR_Announce(FP_ERR_IOCTLFAIL,cmd,result,FPERR_err2str(result));
        break;

    default:
        result = -FP_ERR_BADIOCTL;
        ctl.result = result;
        FPERR_Announce(FP_ERR_BAD_IOCTL,cmd);
    }
    ctl.result = result;
    result = 0;
    // Copy data back to user; including the result 
    memCopyRes = copy_to_user((char *)arg, (char *)&ctl, sizeof(ctl));
    if (memCopyRes != 0) {
        LOG_Log(LOG_ERROR,"driver_ioctl:(3) copy_to_user failed to copy %ul bytes out of %d\n",
            memCopyRes, sizeof(ctl));
        result = -EINVAL;
    }

    if (buffer) MEM_free(buffer);
    TRACE_Log(TRACE_HOOK,"driver_ioctl exit res:%d\n",result);
    return result;
}

int driver_open(struct inode *inode, struct file *filp)
{
    if (unload_in_progress) return -1;
    return 0; 
}

int driver_release (struct inode *inode, struct file *filp)
{
    return 0;
}

/****************************************************************************************************/
/******* Module Functions ***************************************************************************/
/****************************************************************************************************/

struct file_operations fp_fops = {
# if ( LINUX_VERSION_CODE > KERNEL_VERSION(2,6,35) ) 
unlocked_ioctl   : driver_ioctl,
#else
ioctl            : driver_ioctl,
#endif
open             : driver_open,
release          : driver_release,
};

int init_module(void)
    /*
    * Linux calls this function as it loads the driver.
    */
{
    int result = 0;

    init_rwsem(&hook_sem);
    LOG_SetTarget(PF_SysLog);

# if ( LINUX_VERSION_CODE >= KERNEL_VERSION(3,3,0) )
    if (mountStructVerify() == _FALSE)
    {
        LOG_Log(LOG_ERROR,"init_module: The mount structure does not match the currently running kernel version. %d\n",-1);
        return -1;
    }
#endif
    /*
    * I use a couple of hashes.  Init them.
    */
    result = HASH_Create(HASH_Type_Address_e, 0, 0, &hash_id, "hlrp");
    if (result) {
        LOG_Log(LOG_ERROR,"init_module: failed to create hash:%d\n",result);
        return -1;
    }
    result = HASH_Create(HASH_Type_Address_e, 0, 0, &fops_hash,"fops");
    if (result) {
        LOG_Log(LOG_ERROR,"init_module: failed to create ops hash:%d\n",result);
        return -1;
    }
    /* Initialize JBB */
    result = IR_init();
    if (result == 0) result = audit_init(jbbMaxBuffer,jbbAllowWaitIfFull);
    if (result == 0) result = watch_init();
    if (result == 0) result = monitor_init();
    if (result != 0) return -1;
    /*
    * Now, continue with regular module-load stuff.
    * Register your major, and accept a dynamic number
    */
    result = register_chrdev(jbb_major, DEVICE_NAME, &fp_fops);
    if (result < 0) {
        LOG_Log(LOG_ERROR, "init_module: Can't get jbb_major %d\n",result);
        return result;
    }
    if (jbb_major == 0) jbb_major = result; /* dynamic */
    FPERR_Announce(FP_INFO_READY);
    return 0; /* success */
}


void cleanup_module(void) {
    /*
    * This gets called by Linux when the driver is
    * signalled to unload.
    */
    /*
    * VERY IMPORTANT. Before we actually start to undo
    * anything, set a global so that all the other routines
    * here can be aware of an exit in-process.
    */
    unload_in_progress=1;
    FPERR_Announce(FP_INFO_UNLOADING);
    /*
    * We have to be careful. We want to stop any new transactions
    * from starting. So, we unhook everything...except close/release
    * (so that any in-progress files can get out).
    */
    unhook_fs(EXCEPT_CLOSE);
    PF_DelaySeconds(8);
    /*
    * Everything is settled down. We can now
    * completely restore the VFS.
    */
    unhook_fs(EVEN_CLOSE);
    /*
    * There is now no threat of any VFS call getting
    * to us; cleanup our hashes.
    */
    down_write(&hook_sem);
    if (hash_id) HASH_DisposeAndClear(&hash_id);
    if (fops_hash) HASH_DisposeAndClear(&fops_hash);
    up_write(&hook_sem);
    /*
    * Finish with regular module unload stuff.
    */
    unregister_chrdev(jbb_major, DEVICE_NAME);
    /*
    * Shut down tsmjbb
    */
    IR_fini();
    audit_fini();
    watch_fini();
    monitor_fini();  // pf.c
    flush_scheduled_work(); // flush work queues create in pf.c
    error_fini();  
    /*
    * Since we're done with the FP memory system,
    * we can release all of our allocations back
    * to the real OS.
    */ 
    MEM_destroy();    
    LOG_Log(LOG_PRINT, "FILEPATH: Module Cleanup Complete\n","");
}

