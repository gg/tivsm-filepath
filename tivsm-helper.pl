#!/usr/bin/perl

=head1 NAME

  tivsm-helper.pl

=head1 DESCRIPTION

=head2 display current packages, when they are installed

  ./tivsm-helper.pl

=head2 install the packages

  ./tivsm-helper.pl install

=head2 remove the packages

ATTN: use at own risk!

  ./tivsm-helper.pl remove

=cut

use strict;

my %packages= (
  'gskcrypt64'     => 'gskcrypt64_8.0-50.52.linux.x86_64.deb',
  'gskssl64'       => 'gskssl64_8.0-50.52.linux.x86_64.deb',
  'tivsm-filepath' => 'tivsm-filepath-7.1.4-0.deb',
  'tivsm-api64'    => 'tivsm-api64.amd64.deb',
  'tivsm-apicit'   => 'tivsm-apicit.amd64.deb',
  'tivsm-ba'       => 'tivsm-ba.amd64.deb',
  'tivsm-bacit'    => 'tivsm-bacit.amd64.deb',
  'tivsm-bahdw'    => 'tivsm-bahdw.amd64.deb',
  'tivsm-jbb'      => 'tivsm-jbb.amd64.deb'
);

# the order of packages seems to be very important for the installation!
my @packages= qw(
  gskcrypt64
  gskssl64
  tivsm-filepath
  tivsm-api64
  tivsm-apicit
  tivsm-ba
  tivsm-bacit
  tivsm-bahdw
  tivsm-jbb
);

# print "packages: [", join (' ', @packages), "]\n";

my %op_modes= map { $_ => 1 } qw(show remove install);
my $op_mode= 'show';

while (my $arg= shift (@ARGV))
{
  if (exists($op_modes{$arg})) { $op_mode= $arg; }
}

  if ($op_mode eq 'show')
  {
    system (qw(dpkg -l gsk* tivsm*));
  }
  elsif ($op_mode eq 'remove')
  {
  }
  elsif ($op_mode eq 'install')
  {
    foreach my $pkg (@packages)
    {
      system ('dpkg', '--install', $packages{$pkg});
    }
  }

